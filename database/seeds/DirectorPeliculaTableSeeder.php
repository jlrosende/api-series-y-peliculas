<?php

use Illuminate\Database\Seeder;

class DirectorPeliculaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('director_pelicula')->insert(['pelicula_id' => 3, 'director_id' => 1]);
        DB::table('director_pelicula')->insert(['pelicula_id' => 4, 'director_id' => 1]);
        DB::table('director_pelicula')->insert(['pelicula_id' => 5, 'director_id' => 1]);
        DB::table('director_pelicula')->insert(['pelicula_id' => 7, 'director_id' => 2]);
    }
}
