<?php

use Illuminate\Database\Seeder;

class DirectorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        DB::table('directores')->insert(['persona_id' => 3]);
        // 2
        DB::table('directores')->insert(['persona_id' => 4]);
    }
}
