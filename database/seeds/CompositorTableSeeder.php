<?php

use Illuminate\Database\Seeder;

class CompositorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compositores')->insert(['persona_id' => 1]);
        DB::table('compositores')->insert(['persona_id' => 7]);
        DB::table('compositores')->insert(['persona_id' => 8]);
    }
}
