<?php

use Illuminate\Database\Seeder;

class BandaSonoraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bandas_sonoras')->insert(['nombre' => 'El señor de los anillos: La comunidad del anillo', 'compositor_id' => 1]);
        DB::table('bandas_sonoras')->insert(['nombre' => 'El Señor de los Anillos: Las dos torres', 'compositor_id' => 1]);
        DB::table('bandas_sonoras')->insert(['nombre' => 'El señor de los anillos: El retorno del rey', 'compositor_id' => 1]);
    }
}
