<?php

use Illuminate\Database\Seeder;

class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('generos')->insert(['nombre' => 'Drama']);
        DB::table('generos')->insert(['nombre' => 'Comedia']);
        DB::table('generos')->insert(['nombre' => 'Acción']);
        DB::table('generos')->insert(['nombre' => 'Aventura']);
        DB::table('generos')->insert(['nombre' => 'Terror']);
        DB::table('generos')->insert(['nombre' => 'Romance']);
        DB::table('generos')->insert(['nombre' => 'Musical']);
        DB::table('generos')->insert(['nombre' => 'Melodrama']);
        DB::table('generos')->insert(['nombre' => 'Suspense']);
        DB::table('generos')->insert(['nombre' => 'Histórico']);
        DB::table('generos')->insert(['nombre' => 'Bélico']);
        DB::table('generos')->insert(['nombre' => 'Policiaco']);
        DB::table('generos')->insert(['nombre' => 'Western']);
        DB::table('generos')->insert(['nombre' => 'Fantasia']);
        DB::table('generos')->insert(['nombre' => 'Ciencia Ficción']);
        DB::table('generos')->insert(['nombre' => 'Cine Negro']);
        DB::table('generos')->insert(['nombre' => 'Serie B']);
        DB::table('generos')->insert(['nombre' => 'Cine Mudo']);
        DB::table('generos')->insert(['nombre' => 'De Culto']);
        DB::table('generos')->insert(['nombre' => 'Animación']);
        DB::table('generos')->insert(['nombre' => 'Independiente']);
        DB::table('generos')->insert(['nombre' => 'Documental']);
        DB::table('generos')->insert(['nombre' => 'Thriller']);
    }
}
