<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();


        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(ConnectRelationshipsSeeder::class);

        $this->call(UsersTableSeeder::class);

        $this->call(PerfilesTableSeeder::class);
        //$this->call(CategoriaTableSeeder::class);
        //$this->call(PaisTableSeeder::class);
        //$this->call(PersonaTableSeeder::class);
        //$this->call(CompositorTableSeeder::class);
        //$this->call(BandaSonoraTableSeeder::class);
        //$this->call(PeliculaTableSeeder::class);
        //$this->call(SerieTableSeeder::class);
        //$this->call(ActorTableSeeder::class);
        //$this->call(DirectorTableSeeder::class);

        //$this->call(ActorPeliculaTableSeeder::class);
        //$this->call(DirectorPeliculaTableSeeder::class);

        Model::reguard();
    }
}
