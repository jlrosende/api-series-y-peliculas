<?php

use Illuminate\Database\Seeder;

class PersonaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        DB::table('personas')->insert(['nombre' => 'Howard Shore', 'fecha_nacimiento' => '1946-10-18', 'fecha_defuncion' => null]);
        // 2
        DB::table('personas')->insert(['nombre' => 'Viggo Mortensen', 'fecha_nacimiento' => '1958-10-20', 'fecha_defuncion' => null]);
        // 3
        DB::table('personas')->insert(['nombre' => 'Peter Jackson', 'fecha_nacimiento' => '1961-10-31', 'fecha_defuncion' => null]);
        // 4
        DB::table('personas')->insert(['nombre' => 'Francis Lawrence', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 5
        DB::table('personas')->insert(['nombre' => 'Kevin Brodbin', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 6
        DB::table('personas')->insert(['nombre' => 'Frank Cappello', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 7
        DB::table('personas')->insert(['nombre' => 'Brian Tyler', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 8
        DB::table('personas')->insert(['nombre' => 'Klaus Badelt', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 9
        DB::table('personas')->insert(['nombre' => 'Keanu Reeves', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 10
        DB::table('personas')->insert(['nombre' => 'Rachel Weisz', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 11
        DB::table('personas')->insert(['nombre' => 'Shia LaBeouf', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 12
        DB::table('personas')->insert(['nombre' => 'Tilda Swinton', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 13
        DB::table('personas')->insert(['nombre' => 'Pruitt Taylor Vince', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 14
        DB::table('personas')->insert(['nombre' => 'Djimon Hounsou', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 15
        DB::table('personas')->insert(['nombre' => 'Gavin Rossdale', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 16
        DB::table('personas')->insert(['nombre' => 'Peter Stormare', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 17
        DB::table('personas')->insert(['nombre' => 'Michelle Monaghan', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 18
        DB::table('personas')->insert(['nombre' => 'José Zúñiga', 'fecha_nacimiento' => null, 'fecha_defuncion' => null]);
        // 19
        DB::table('personas')->insert(['nombre' => 'Elijah Wood']);
        // 20
        DB::table('personas')->insert(['nombre' => 'Ian McKellen']);
        // 21
        DB::table('personas')->insert(['nombre' => 'Orlando Bloom']);
        // 22 
        DB::table('personas')->insert(['nombre' => 'Sean Astin']);
        // 23
        DB::table('personas')->insert(['nombre' => 'Billy Boyd']);
    }
}
