<?php

use Illuminate\Database\Seeder;

class ActorPeliculaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actor_pelicula')->insert(['pelicula_id' => 3, 'actor_id' => 1]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 3, 'actor_id' => 12]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 3, 'actor_id' => 13]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 3, 'actor_id' => 14]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 3, 'actor_id' => 15]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 3, 'actor_id' => 16]);

        DB::table('actor_pelicula')->insert(['pelicula_id' => 4, 'actor_id' => 1]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 4, 'actor_id' => 12]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 4, 'actor_id' => 13]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 4, 'actor_id' => 14]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 4, 'actor_id' => 15]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 4, 'actor_id' => 16]);

        DB::table('actor_pelicula')->insert(['pelicula_id' => 5, 'actor_id' => 1]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 5, 'actor_id' => 12]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 5, 'actor_id' => 13]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 5, 'actor_id' => 14]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 5, 'actor_id' => 15]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 5, 'actor_id' => 16]);

        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 2]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 3]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 4]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 5]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 6]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 7]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 8]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 9]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 10]);
        DB::table('actor_pelicula')->insert(['pelicula_id' => 7, 'actor_id' => 11]);
    }
}
