<?php

use Illuminate\Database\Seeder;
use SeriesYPeliculas\Perfil;

class PerfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Perfil::create(['user_id' => 1, 'nombre' => 'Jorge', 'apellidos' => 'Lopez Rosende']);
        Perfil::create(['user_id' => 2, 'nombre' => 'Jorge', 'apellidos' => 'elRizos']);
    }
}
