<?php

use Illuminate\Database\Seeder;

class ActorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        DB::table('actores')->insert(['persona_id' => 2]);
        // 2
        DB::table('actores')->insert(['persona_id' => 9]);
        // 3
        DB::table('actores')->insert(['persona_id' => 10]);
        // 4
        DB::table('actores')->insert(['persona_id' => 11]);
        // 5
        DB::table('actores')->insert(['persona_id' => 12]);
        // 6
        DB::table('actores')->insert(['persona_id' => 13]);
        // 7
        DB::table('actores')->insert(['persona_id' => 14]);
        // 8
        DB::table('actores')->insert(['persona_id' => 15]);
        // 9
        DB::table('actores')->insert(['persona_id' => 16]);
        // 10
        DB::table('actores')->insert(['persona_id' => 17]);
        // 11
        DB::table('actores')->insert(['persona_id' => 18]);
        // 12
        DB::table('actores')->insert(['persona_id' => 19]);
        // 13
        DB::table('actores')->insert(['persona_id' => 20]);
        // 14
        DB::table('actores')->insert(['persona_id' => 21]);
        // 15
        DB::table('actores')->insert(['persona_id' => 22]);
        // 16
        DB::table('actores')->insert(['persona_id' => 23]);
         
    }
}
