<?php

use Illuminate\Database\Seeder;

class PaisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paises')->insert(['nombre' => 'España', 'siglas' => 'ES']);
        DB::table('paises')->insert(['nombre' => 'Francia', 'siglas' => 'FR']);
        DB::table('paises')->insert(['nombre' => 'Estados Unidos', 'siglas' => 'US']);
        DB::table('paises')->insert(['nombre' => 'Alemania', 'siglas' => 'DE']);
        DB::table('paises')->insert(['nombre' => 'Reino Unido', 'siglas' => 'GB']);
        DB::table('paises')->insert(['nombre' => 'Italia', 'siglas' => 'IT']);
        DB::table('paises')->insert(['nombre' => 'Portugal', 'siglas' => 'PT']);
        DB::table('paises')->insert(['nombre' => 'Argentina', 'siglas' => 'AR']);
        DB::table('paises')->insert(['nombre' => 'China', 'siglas' => 'CN']);
        DB::table('paises')->insert(['nombre' => 'Japón', 'siglas' => 'JP']);
    }
}
