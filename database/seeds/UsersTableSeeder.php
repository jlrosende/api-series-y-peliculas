<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $appRole = config('roles.models.role')::where('name', '=', 'App')->first();
        $userRole = config('roles.models.role')::where('name', '=', 'User')->first();
        $adminRole = config('roles.models.role')::where('name', '=', 'Admin')->first();
        $permissions = config('roles.models.permission')::all();

        /*
         * Add Users
         *
         */
        if (config('roles.models.defaultUser')::where('email', '=', 'jorgelopezrosende@hotmail.com')->first() === null) {
            $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'jorge lopez rosende',
                'email'    => 'jorgelopezrosende@hotmail.com',
                'password' => bcrypt('test'),
            ]);

            $newUser->attachRole($adminRole);
            foreach ($permissions as $permission) {
                $newUser->attachPermission($permission);
            }
        }

        if (config('roles.models.defaultUser')::where('email', '=', 'jorgelrizos@gmail.com')->first() === null) {
            $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'rosende95',
                'email'    => 'jorgelrizos@gmail.com',
                'password' => bcrypt('test'),
            ]);

            $newUser;
            $newUser->attachRole($userRole);
        }

        if (config('roles.models.defaultUser')::where('email', '=', 'app@api.peliculas.com')->first() === null) {
            $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'app',
                'email'    => 'app@api.peliculas.com',
                'password' => bcrypt('test'),
            ]);

            $newUser;
            $newUser->attachRole($appRole);
        }
    }
}
