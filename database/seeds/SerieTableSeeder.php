<?php

use Illuminate\Database\Seeder;

class SerieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('series')->insert(['nombre' => 'Fringe', 'anio' => '2008-09-09', 'cartel' => null,
            'sinopsis' => 'La agente del FBI Olivia Dunham y su jefe, el agente Broyles, se encargan de esclarecer fenómenos inexplicables. En sus investigaciones, cuentan con la ayuda del doctor Bishop, un científico despistado que sabe más de lo que parece, y de su hijo Peter.Serie del creador de "Lost" (J.J. Abrams). El episodio piloto, dirigido por Alex Graves, tuvo una buena acogida por parte de la crítica. Al igual que el piloto de "Lost", costó unos 10 millones de dólares. ']);
        DB::table('series')->insert(['nombre' => 'Breaking Bad', 'anio' => '2008-01-20', 'cartel' => null,
            'sinopsis' => 'Tras cumplir 50 años, Walter White (Bryan Cranston), un profesor de química de un instituto de Albuquerque, Nuevo México, se entera de que tiene un cáncer de pulmón incurable. Casado con Skyler (Anna Gunn) y con un hijo discapacitado (RJ Mitte), la brutal noticia lo impulsa a dar un drástico cambio a su vida: decide, con la ayuda de un antiguo alumno (Aaron Paul), fabricar anfetaminas y ponerlas a la venta. Lo que pretende es liberar a su familia de problemas económicos cuando se produzca el fatal desenlace. ']);

    }
}
