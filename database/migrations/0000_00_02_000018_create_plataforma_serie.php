<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlataformaSerie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plataforma_serie', function (Blueprint $table) {
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('serie_id');
            $table->primary(['plataforma_id', 'serie_id']);
            $table->foreign('plataforma_id')->references('id')->on('plataformas')->onDelete('cascade');
            $table->foreign('serie_id')->references('id')->on('series')->onDelete('cascade');
            $table->string('url')->nullable();
            $table->date('inicio_disponible')->nullable();
            $table->date('fin_disponible')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plataforma_serie');
    }
}
