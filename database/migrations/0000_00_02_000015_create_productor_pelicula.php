<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductorPelicula extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelicula_productor', function (Blueprint $table) {
            $table->unsignedBigInteger('productor_id');
            $table->unsignedBigInteger('pelicula_id');
            $table->primary(['productor_id', 'pelicula_id']);
            $table->foreign('productor_id')->references('id')->on('productores')->onDelete('cascade');
            $table->foreign('pelicula_id')->references('id')->on('peliculas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelicula_productor');
    }
}
