<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlataformaPelicula extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plataforma_pelicula', function (Blueprint $table) {
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('pelicula_id');
            $table->primary(['plataforma_id', 'pelicula_id']);
            $table->foreign('plataforma_id')->references('id')->on('plataformas')->onDelete('cascade');
            $table->foreign('pelicula_id')->references('id')->on('peliculas')->onDelete('cascade');
            $table->string('url')->nullable();
            $table->date('inicio_disponible')->nullable();
            $table->date('fin_disponible')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plataforma_pelicula');
    }
}
