<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductorSerie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serie_productor', function (Blueprint $table) {
            $table->unsignedBigInteger('productor_id');
            $table->unsignedBigInteger('serie_id');
            $table->primary(['productor_id', 'serie_id']);
            $table->foreign('productor_id')->references('id')->on('productores')->onDelete('cascade');
            $table->foreign('serie_id')->references('id')->on('series')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serie_productor');
    }
}
