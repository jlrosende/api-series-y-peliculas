<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeliculasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peliculas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 50);
            $table->date('anio')->nullable();
            $table->text('sinopsis')->nullable();
            $table->time('duracion')->nullable();
            $table->string('cartel')->nullable();
            $table->unsignedBigInteger('banda_sonora_id')->nullable();
            $table->foreign('banda_sonora_id')->references('id')->on('bandas_sonoras')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peliculas');
    }
}
