<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->date('anio')->nullable();
            $table->text('sinopsis')->nullable();
            $table->string('cartel')->nullable();
            $table->unsignedBigInteger('banda_sonora_id')->nullable();
            $table->foreign('banda_sonora_id')->references('id')->on('bandas_sonoras');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series');
    }
}
