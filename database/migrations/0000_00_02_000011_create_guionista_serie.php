<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuionistaSerie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guionista_serie', function (Blueprint $table) {
            $table->unsignedBigInteger('serie_id');
            $table->unsignedBigInteger('guionista_id');
            $table->primary(['serie_id', 'guionista_id']);
            $table->foreign('serie_id')->references('id')->on('series')->onDelete('cascade');
            $table->foreign('guionista_id')->references('id')->on('guionistas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guionista_serie');
    }
}
