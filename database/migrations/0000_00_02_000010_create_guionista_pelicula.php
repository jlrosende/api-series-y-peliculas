<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuionistaPelicula extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guionista_pelicula', function (Blueprint $table) {
            $table->unsignedBigInteger('pelicula_id');
            $table->unsignedBigInteger('guionista_id');
            $table->primary(['pelicula_id', 'guionista_id']);
            $table->foreign('pelicula_id')->references('id')->on('peliculas')->onDelete('cascade');
            $table->foreign('guionista_id')->references('id')->on('guionistas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guionista_pelicula');
    }
}
