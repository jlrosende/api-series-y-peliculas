<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremiados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premiados', function (Blueprint $table) {
            $table->year('anio');
            $table->unsignedInteger('edicion');
            $table->enum('estado', ['nominado', 'premiado']);
            $table->morphs('premiado');
            $table->unsignedBigInteger('festival_id');
            $table->unsignedBigInteger('premio_id');
            $table->primary(['festival_id', 'premio_id', 'anio', 'edicion']);
            $table->foreign('festival_id')->references('id')->on('festivales')->onDelete('cascade');
            $table->foreign('premio_id')->references('id')->on('premios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premiados');
    }
}
