<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

use Illuminate\Support\Facades\Storage;

class Perfil extends Model
{
    use SoftDeletes;

    protected $table = 'perfiles';
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['nombre', 'apellidos', 'avatar', 'telefono', 'cpostal', 'direccion'];

    public function user(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\User');
    }

    public function peliculas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Pelicula', 'perfil_pelicula')->withPivot('estado');
    }

    public function series(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Serie', 'perfil_serie')->withPivot('estado');
    }

    public function avatar($perfil)
    {
        if ($perfil->avatar)
            return Storage::disk('public')->url($perfil->avatar);
        return null;
    }

    public function n_peliculas($perfil)
    {
        return $perfil->peliculas()->count();
    }

    public function n_series($perfil)
    {
        return $perfil->series()->count();
    }
}
