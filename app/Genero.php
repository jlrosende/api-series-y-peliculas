<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Genero extends Model
{
    protected $table = 'generos';

    public function peliculas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Pelicula');
    }

    public function series(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Serie');
    }
}
