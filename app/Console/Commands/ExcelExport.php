<?php

namespace SeriesYPeliculas\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Filesystem\Filesystem;

use SeriesYPeliculas\Exports\ActoresExport;
use SeriesYPeliculas\Exports\BandasSonorasExport;
use SeriesYPeliculas\Exports\CancionExport;
use SeriesYPeliculas\Exports\CapitulosExport;
use SeriesYPeliculas\Exports\CompositoresExport;
use SeriesYPeliculas\Exports\DirectoresExport;
use SeriesYPeliculas\Exports\FestivalesExport;
use SeriesYPeliculas\Exports\GenerosExport;
use SeriesYPeliculas\Exports\GuionistasExport;
use SeriesYPeliculas\Exports\PaisesExport;
use SeriesYPeliculas\Exports\PeliculasExport;
use SeriesYPeliculas\Exports\PerfilesExport;
use SeriesYPeliculas\Exports\PermissionsExport;
use SeriesYPeliculas\Exports\PersonasExport;
use SeriesYPeliculas\Exports\PlataformasExport;
use SeriesYPeliculas\Exports\PremiosExport;
use SeriesYPeliculas\Exports\ProductoresExport;
use SeriesYPeliculas\Exports\RolesExport;
use SeriesYPeliculas\Exports\SeriesExport;
use SeriesYPeliculas\Exports\TemporadasExport;
use SeriesYPeliculas\Exports\UsersExport;
use Illuminate\Support\Carbon;

class ExcelExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "excel:export";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Exporta toda la base de datos a excel";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Save types:");
        if (Excel::store(new ActoresExport, "actores.csv", "excel")) {
            $this->line("\tActores saved.");
        } else {
            $this->error("ERROR: Actores can\'t be saved.");
        }

        if (Excel::store(new BandasSonorasExport, "bandas_sonoras.csv", "excel")) {
            $this->line("\tBandas Sonoras saved.");
        } else {
            $this->error("ERROR: Bandas Sonoras can\'t be saved.");
        }

        if (Excel::store(new CancionExport, "canciones.csv", "excel")) {
            $this->line("\tCanciones saved.");
        } else {
            $this->error("ERROR: Canciones can\'t be saved.");
        }

        if (Excel::store(new CapitulosExport, "capitulos.csv", "excel")) {
            $this->line("\tCapitulos saved.");
        } else {
            $this->error("ERROR: Capitulos can\'t be saved.");
        }

        if (Excel::store(new CompositoresExport, "compositores.csv", "excel")) {
            $this->line("\tCompositores saved.");
        } else {
            $this->error("ERROR: Compositores can\'t be saved.");
        }

        if (Excel::store(new DirectoresExport, "directores.csv", "excel")) {
            $this->line("\tDirectores saved.");
        } else {
            $this->error("ERROR: Directores can\'t be saved.");
        }

        if (Excel::store(new FestivalesExport, "festivales.csv", "excel")) {
            $this->line("\tFestivales saved.");
        } else {
            $this->error("ERROR: Festivales can\'t be saved.");
        }

        if (Excel::store(new GenerosExport, "generos.csv", "excel")) {
            $this->line("\tGeneros saved.");
        } else {
            $this->error("ERROR: Generos can\'t be saved.");
        }

        if (Excel::store(new GuionistasExport, "guionistas.csv", "excel")) {
            $this->line("\tGuionistas saved.");
        } else {
            $this->error("ERROR: Guionistas can\'t be saved.");
        }

        if (Excel::store(new PaisesExport, "paises.csv", "excel")) {
            $this->line("\tPaises saved.");
        } else {
            $this->error("ERROR: Paises can\'t be saved.");
        }

        if (Excel::store(new PeliculasExport, "peliculas.csv", "excel")) {
            $this->line("\tPeliculas saved.");
        } else {
            $this->error("ERROR: Peliculas can\'t be saved.");
        }

        if (Excel::store(new PerfilesExport, "perfiles.csv", "excel")) {
            $this->line("\tPerfiles saved.");
        } else {
            $this->error("ERROR: Perfiles can\'t be saved.");
        }

        if (Excel::store(new PermissionsExport, "permissions.csv", "excel")) {
            $this->line("\tPermissions saved.");
        } else {
            $this->error("ERROR: Permissions can\'t be saved.");
        }

        if (Excel::store(new PersonasExport, "personas.csv", "excel")) {
            $this->line("\tPersonas saved.");
        } else {
            $this->error("ERROR: Personas can\'t be saved.");
        }

        if (Excel::store(new PlataformasExport, "plataformas.csv", "excel")) {
            $this->line("\tPlataformas saved.");
        } else {
            $this->error("ERROR: Plataformas can\'t be saved.");
        }

        if (Excel::store(new PremiosExport, "premios.csv", "excel")) {
            $this->line("\tPremios saved.");
        } else {
            $this->error("ERROR: Premios can\'t be saved.");
        }

        if (Excel::store(new ProductoresExport, "productores.csv", "excel")) {
            $this->line("\tProductores saved.");
        } else {
            $this->error("ERROR: Productores can\'t be saved.");
        }

        if (Excel::store(new RolesExport, "roles.csv", "excel")) {
            $this->line("\tRoles saved.");
        } else {
            $this->error("ERROR: Roles can\'t be saved.");
        }

        if (Excel::store(new SeriesExport, "series.csv", "excel")) {
            $this->line("\tSeries saved.");
        } else {
            $this->error("ERROR: Series can\'t be saved.");
        }

        if (Excel::store(new TemporadasExport, "temporadas.csv", "excel")) {
            $this->line("\tTemporadas saved.");
        } else {
            $this->error("ERROR: Temporadas can\'t be saved.");
        }

        if (Excel::store(new UsersExport, "users.csv", "excel")) {
            $this->line("\tUsers saved.");
        } else {
            $this->error("ERROR: Users can\'t be saved.");
        }

        $this->info("Save relations:");

        $this->info("Save images:");
        $this->info(storage_path('app/public/'));
        $this->info(storage_path('app/excel/img/'));
        $fs = new Filesystem();
        $public = storage_path('app/public/');
        $excel = storage_path('app/excel/img');
        $fs->copyDirectory($public, $excel);
        $this->info("Compress data:");
        $dt = Carbon::now();
        $zip_file = base_path('saves/') . $dt->toDateTimeString() . '.zip';
        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $path = storage_path('app/excel/');
        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        foreach ($files as $name => $file) {
            // We're skipping all subfolders
            if (!$file->isDir()) {
                $filePath     = $file->getRealPath();

                // extracting filename with substr/strlen
                $relativePath = 'excel/' . substr($filePath, strlen($path));

                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        $this->info("DONE.");
    }
}
