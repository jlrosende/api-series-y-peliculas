<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Storage;

class Persona extends Model
{
    protected $table = 'personas';

    public function actor(): HasOne
    {
        return $this->hasOne('SeriesYPeliculas\Actor');
    }

    public function director(): HasOne
    {
        return $this->hasOne('SeriesYPeliculas\Director');
    }

    public function guionista(): HasOne
    {
        return $this->hasOne('SeriesYPeliculas\Guionista');
    }

    public function compositor(): HasOne
    {
        return $this->hasOne('SeriesYPeliculas\Compositor');
    }

    public function productor(): HasOne
    {
        return $this->hasOne('SeriesYPeliculas\Productor');
    }

    public function paises(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Pais', 'pais_persona');
    }

    public function foto($persona)
    {
        if ($persona->foto)
            return Storage::disk('public')->url($persona->foto);
        return null;
    }
}
