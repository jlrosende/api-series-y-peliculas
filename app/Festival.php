<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Festival extends Model
{
    protected $table = 'festivales';

    public function peliculas(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Pelicula', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function series(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Serie', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function actores(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Actor', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function guionistas(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Guonista', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function diretores(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Director', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function compositores(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Compositor', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function pais(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\Pais');
    }

    public function premios(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Premio', 'premiados', 'festival_id', 'premio_id')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function anios($festival)
    {
        return collect($festival->premios()->get())->map(function ($item) {
            return $item->pivot->anio;
        })->unique();
    }

    public function ediciones($festival)
    {
        return collect($festival->premios()->get())->map(function ($item) {
            return $item->pivot->edicion;
        })->unique();
    }
}
