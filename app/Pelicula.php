<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Pelicula extends Model
{
    protected $table = 'peliculas';
    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = ['nombre', 'anio', 'sinopsis', 'duracion', 'cartel'];

    public function paises(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Pais', 'pais_pelicula');
    }

    public function generos(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Genero');
    }

    public function actores(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Actor');
    }

    public function directores(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Director');
    }

    public function guionistas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Guionista');
    }

    public function productores(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Productor');
    }

    public function banda_sonora(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\BandaSonora');
    }

    public function perfiles(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Perfil', 'perfil_pelicula')->withPivot('estado');
    }

    public function plataformas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Plataforma', 'plataforma_pelicula')->withPivot(['url', 'inicio_disponible', 'fin_disponible']);
    }

    public function premios(): MorphToMany
    {
        return $this->morphToMany('SeriesYPeliculas\Premio', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function festivales(): MorphToMany
    {
        return $this->morphToMany('SeriesYPeliculas\Festival', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function cartel($pelicula)
    {
        if ($pelicula->cartel)
            return Storage::disk('public')->url($pelicula->cartel);
        return null;
    }
}
