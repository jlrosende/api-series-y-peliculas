<?php

namespace SeriesYPeliculas\Policies;

use SeriesYPeliculas\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @param  \SeriesYPeliculas\User  $model
     * @return mixed
     */
    public function views(User $user, array $model)
    {
        return $user->isAdmin() || $user->id == $model->id;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @param  \SeriesYPeliculas\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        return $user->isAdmin() || $user->id == $model->id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @param  \SeriesYPeliculas\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        return $user->isAdmin() || $user->id == $model->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @param  \SeriesYPeliculas\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        return $user->isAdmin() || $user->id == $model->id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @param  \SeriesYPeliculas\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        return $user->isAdmin() || $user->id == $model->id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @param  \SeriesYPeliculas\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        return $user->isAdmin();
    }
}
