<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Pais extends Model
{
    protected $table = 'paises';

    public function peliculas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Pelicula', 'pais_pelicula');
    }

    public function series(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Serie', 'pais_serie');
    }

    public function personas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Perosnas', 'pais_persona');
    }

    public function festivales(): HasMany
    {
        return $this->hasMany('SeriesYPeliculas\Festival');
    }
}
