<?php

namespace SeriesYPeliculas\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use jeremykenedy\LaravelRoles\Models\Role;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RolesExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'name',
            'slug',
            'description',
            'level',
            'created_at',
            'updated_at',
            'deleted_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Role::all();
    }
}
