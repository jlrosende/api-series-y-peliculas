<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Compositor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompositoresExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'persona_id',
            'created_at',
            'updated_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Compositor::all();
    }
}
