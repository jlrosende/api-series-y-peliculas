<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Perfil;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PerfilesExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'nombre',
            'apellidos',
            'avatar',
            'telefono',
            'cpostal',
            'direccion',
            'created_at',
            'updated_at',
            'deleted_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Perfil::all();
    }
}
