<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Productor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductoresExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'persona_id',
            'created_at',
            'updated_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Productor::all();
    }
}
