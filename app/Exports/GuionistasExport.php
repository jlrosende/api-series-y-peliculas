<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Guionista;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class GuionistasExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'persona',
            'created_at',
            'updated_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Guionista::all();
    }
}
