<?php

namespace SeriesYPeliculas\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use jeremykenedy\LaravelRoles\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PermissionsExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'name',
            'slug',
            'description',
            'model',
            'created_at',
            'updated_at',
            'deleted_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Permission::all();
    }
}
