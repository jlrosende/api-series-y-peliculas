<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{   
    public function headings(): array
    {
        return [
            '#',
            'name',
            'email',
            'password',
            'remmember_token',
            'created_at',
            'updated_at',
            'deleted_at'
        ];
    }

    public function collection()
    {
        return User::all()->makeVisible(['password', 'remmember_token']);
    }
}