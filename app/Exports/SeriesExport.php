<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Serie;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SeriesExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'nombre',
            'anio',
            'sinopsis',
            'cartel',
            'banda_sonora_id',
            'created_at',
            'updated_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Serie::all();
    }
}
