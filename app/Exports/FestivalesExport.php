<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Festival;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FestivalesExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'nombre',
            'pais_id',
            'created_at',
            'updated_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Festival::all();
    }
}
