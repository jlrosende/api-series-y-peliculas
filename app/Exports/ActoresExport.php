<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Actor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ActoresExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'persona_id',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Actor::all();
    }
}
