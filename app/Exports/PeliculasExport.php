<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Pelicula;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PeliculasExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'nombre',
            'anios',
            'sinopsis',
            'duracion',
            'cartel',
            'banda_sonora_id',
            'created_at',
            'updated_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Pelicula::all();
    }
}
