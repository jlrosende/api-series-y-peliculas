<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Genero;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class GenerosExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'nombre',
            'created_at',
            'updated_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Genero::all();
    }
}
