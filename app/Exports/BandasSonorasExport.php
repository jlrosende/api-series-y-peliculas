<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\BandaSonora;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BandasSonorasExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'nombre',
            'compositor_id',
            'created_at',
            'updated_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return BandaSonora::all();
    }
}
