<?php

namespace SeriesYPeliculas\Exports;

use SeriesYPeliculas\Capitulo;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CapitulosExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            '#',
            'nombre',
            'sinopsis',
            'duracion',
            'temporada_id',
            'created_at',
            'updated_at'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Capitulo::all();
    }
}
