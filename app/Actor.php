<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Actor extends Model
{
    protected $table = "actores";

    public function peliculas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Pelicula');
    }

    public function series(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Serie');
    }

    public function persona(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\Persona');
    }

    public function premios(): MorphToMany
    {
        return $this->morphToMany('SeriesYPeliculas\Premio', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function festivales(): MorphToMany
    {
        return $this->morphToMany('SeriesYPeliculas\Festival', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function n_peliculas($actor)
    {
        return $actor->peliculas()->count();
    }

    public function n_series($actor)
    {
        return $actor->series()->count();
    }
}
