<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Productor extends Model
{
    protected $table = 'productores';

    public function persona(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\Persona');
    }

    public function peliculas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Pelicula');
    }

    public function series(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Serie', 'serie_productor');
    }

    public function n_peliculas($productor)
    {
        return $productor->peliculas()->count();
    }

    public function n_series($productor)
    {
        return $productor->series()->count();
    }
}
