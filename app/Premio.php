<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Premio extends Model
{
    protected $table = 'premios';

    public function peliculas(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Pelicula', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function series(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Serie', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function actores(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Actor', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function guionistas(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Guonista', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function diretores(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Director', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function compositores(): MorphToMany
    {
        return $this->morphedByMany('SeriesYPeliculas\Compositor', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function festivales(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Festival', 'premiados', 'premio_id', 'festival_id')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function festival($premio)
    {
        if ($premio->pivot) {
            return $premio->festivales()->where('festival_id', $premio->pivot->festival_id)->first();
        }
        return null;
    }
}
