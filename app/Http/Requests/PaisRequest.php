<?php

namespace SeriesYPeliculas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaisRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'siglas' => 'required|size:2',
        ];
    }

    public function response(array $errors)
    {
        return response()->json(['error' => $errors], 400);
    }
}
