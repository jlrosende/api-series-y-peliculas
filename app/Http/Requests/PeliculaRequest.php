<?php

namespace SeriesYPeliculas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PeliculaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:50',
            'anio' => 'date|nullable',
            'sinopsis' => 'string|nullable',
            'duracion' => 'date_format:h:i|nullable',
            'cartel' => 'file|nullable',
            'banda_sonora_id' => 'integer|nullable'
        ];
    }

    public function response(array $errors)
    {
        return response()->json(['error' => $errors], 400);
    }
}
