<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Capitulo;
use Illuminate\Http\Request;
use SeriesYPeliculas\Http\Requests\CapituloRequest;

class CapituloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $capitulo = Capitulo::paginate(10);
        return response()->json($capitulo, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CapituloRequest $request)
    {
        Capitulo::create([
            'nombre' => $request->nombre,
            'sinopsis' => $request->sinopsis,
            'duracion' => $request->duracion,
            'temporada_id' => $request->temporada_id,
        ]);

        return response()->json(['create' => 'correct'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\Capitulo  $capitulo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $capitulo = Capitulo::find($id);
        return response()->json($capitulo, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\Capitulo  $capitulo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $capitulo = Capitulo::find($id);
        return response()->json($capitulo, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\Capitulo  $capitulo
     * @return \Illuminate\Http\Response
     */
    public function update(CapituloRequest $request, $id)
    {
        $capitulo = Capitulo::find($id);
        $capitulo->nombre = $request->nombre;
        $capitulo->sinopsis = $request->sisnopsis;
        $capitulo->duracion = $request->duracion;
        $capitulo->temporada_id = $request->temporada_id;
        $capitulo->save();

        return response()->json(['update' => 'correct'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\Capitulo  $capitulo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Capitulo::deleted($id);
        return response()->json(['delete' => 'correct'], 200);
    }
}
