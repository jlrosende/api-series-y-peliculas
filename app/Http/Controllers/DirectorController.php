<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Compositor;
use SeriesYPeliculas\Director;
use SeriesYPeliculas\Http\Requests\CompositorRequest;
use SeriesYPeliculas\Http\Requests\DirectorRequest;

class DirectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $director = Director::paginate(10);
        return response()->json($director);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompositorRequest $request)
    {
        Compositor::create([
            'nombre' => $request->nombre,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'fecha_defuncion' => $request->fecha_defuncion,
        ]);

        return response()->json(['create' => 'correct'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $director = Director::find($id);
        return response()->json($director);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $director = Director::find($id);
        return response()->json($director);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function update(DirectorRequest $request, $id)
    {
        $director = Director::find($id);
        $director->nombre = $request->nombre;
        $director->fecha_nacimiento = $request->fecha_nacimiento;
        $director->fecha_defuncion = $request->fecha_defuncion;
        $director->save();

        return response()->json(['update' => 'correct'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Director::deleted($id);
        return response()->json(['delete' => 'correct'], 200);
    }
}
