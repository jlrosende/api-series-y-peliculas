<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Productor;
use Illuminate\Http\Request;

class ProductoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\Productor  $productora
     * @return \Illuminate\Http\Response
     */
    public function show(Productor $productora)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\Productor  $productora
     * @return \Illuminate\Http\Response
     */
    public function edit(Productor $productora)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\Productor  $productora
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Productor $productora)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\Productor  $productora
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productor $productora)
    {
        //
    }
}
