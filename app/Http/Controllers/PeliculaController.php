<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Http\Requests\PeliculaRequest;
use SeriesYPeliculas\Pelicula;
use Illuminate\Http\Request;

class PeliculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pelicula = Pelicula::paginate(10);
        return response()->json($pelicula, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PeliculaRequest $request)
    {
        Pelicula::create([
            'nombre' => $request->nombre,
            'anio' => $request->anio,
            'sinopsis' => $request->sinopsis,
            'duracion' => $request->duracion,
            'cartel' => $request->cartel,
            'banda_sonora' => $request->banda_sonora,
        ]);

        return response()->json(['create' => 'correct'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\Pelicula  $pelicula
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pelicula = Pelicula::find($id);
        return response()->json($pelicula, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\Pelicula  $pelicula
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pelicula = Pelicula::find($id);
        return response()->json($pelicula, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\Pelicula  $pelicula
     * @return \Illuminate\Http\Response
     */
    public function update(PeliculaRequest $request, $id)
    {
        $pelicula = Pelicula::find($id);
        $pelicula->nombre = $request->nombre;
        $pelicula->anio = $request->anio;
        $pelicula->sinopsis = $request->sinopsis;
        $pelicula->duracion = $request->duracion;
        $pelicula->cartel = $request->cartel;
        $pelicula->banda_sonora_id = $request->banda_sonora_id;
        $pelicula->save();

        return response()->json(['update' => 'create'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\Pelicula  $pelicula
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pelicula::deleted($id);
        return response()->json(['delete' => 'correct'], 200);
    }
}
