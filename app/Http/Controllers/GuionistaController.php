<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\guionista;
use Illuminate\Http\Request;
use SeriesYPeliculas\Http\Requests\GuionistaRequest;

class GuionistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guionista = Guionista::paginate(10);
        return response()->json($guionista);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuionistaRequest $request)
    {
        Guionista::create([
            'nombre' => $request->nombre,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'fecha_defuncion' => $request->fecha_defuncion,
        ]);

        return response()->json(['create' => 'correct'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\guionista  $guionista
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $guionista = Guionista::find($id);
        return response()->json($guionista, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\guionista  $guionista
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guionista = Guionista::find($id);
        return response()->json($guionista, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\guionista  $guionista
     * @return \Illuminate\Http\Response
     */
    public function update(GuionistaRequest $request, $id)
    {
        $guionista = Guionista::find($id);
        $guionista->nombre = $request->nombre;
        $guionista->fecha_nacimiento = $request->fecha_nacimiento;
        $guionista->fecha_defuncion = $request->fecha_defuncion;
        $guionista->save();

        return response()->json(['update' => 'correct'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\guionista  $guionista
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Guionista::deleted($id);
        return response()->json(['delete' => 'correct'], 200);
    }
}
