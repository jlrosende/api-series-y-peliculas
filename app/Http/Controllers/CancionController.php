<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Cancion;
use Illuminate\Http\Request;
use SeriesYPeliculas\Http\Requests\CancionRequest;

class CancionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cancion = Cancion::paginate(10);
        return response()->json($cancion, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CancionRequest $request)
    {
        Cancion::create([
            'nombre' => $request->nombre,
            'duracion' => $request->duracion,
            'banda_sonora' => $request->banda_sonora,
        ]);

        return response()->json(['create' => 'correct'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\Cancion  $cancion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cancion = Cancion::find($id);
        return response()->json($cancion, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\Cancion  $cancion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cancion = Cancion::find($id);
        return response()->json($cancion, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\Cancion  $cancion
     * @return \Illuminate\Http\Response
     */
    public function update(CancionRequest $request, $id)
    {
        $cancion = Cancion::find($id);
        $cancion->nombre = $request->nombre;
        $cancion->duracion = $request->duracion;
        $cancion->banda_sonora = $request->banda_sonora;
        $cancion->save();

        return response()->json(['update' => 'correct'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\Cancion  $cancion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cancion::deleted($id);
        return response()->json(['delete' => 'correct', 200]);
    }
}
