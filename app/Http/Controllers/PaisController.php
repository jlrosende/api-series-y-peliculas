<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Http\Requests\PaisRequest;
use SeriesYPeliculas\pais;
use Illuminate\Http\Request;

class PaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pais = Pais::paginate(10);
        return response()->json($pais, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaisRequest $request)
    {
        Pais::create([
            'nombre' => $request->nombre,
            'siglas' => $request->siglas,
        ]);

        return response()->json(['create' => 'correct'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pais = Pais::find($id);
        return response()->json($pais);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pais = Pais::find($id);
        return response()->json($pais);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function update(PaisRequest $request, $id)
    {
        $pais = Pais::find($id);
        $pais->nombre = $request->nombre;
        $pais->siglas = $request->siglas;
        $pais->save();

        return response()->json(['update' => 'correct'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pais::deleted($id);
        return response()->json(['delete' => 'correct'], 200);
    }
}
