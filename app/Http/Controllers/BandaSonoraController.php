<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Banda_Sonora;
use Illuminate\Http\Request;
use SeriesYPeliculas\Http\Requests\BandaSonoraRequest;

class BandaSonoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bandaSonora = Banda_Sonora::paginate(10);
        return response()->json($bandaSonora, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BandaSonoraRequest $request)
    {
        Banda_Sonora::create([
            'nombre' => $request->nombre,
            'compositor' => $request->compositor,
        ]);

        return response()->json(['create' => 'correct'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\Banda_Sonora  $banda_Sonora
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bandaSonora = Banda_Sonora::find($id);
        return response()->json($bandaSonora, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\Banda_Sonora  $banda_Sonora
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bandaSonora = Banda_Sonora::find($id);
        return response()->json($bandaSonora, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\Banda_Sonora  $banda_Sonora
     * @return \Illuminate\Http\Response
     */
    public function update(BandaSonoraRequest $request, $id)
    {
        $bandaSonora = Banda_Sonora::find($id);
        $bandaSonora->nombre = $request->nombre;
        $bandaSonora->compositor = $request->compositor;
        $bandaSonora->save();

        return response()->json(['update' => 'correct'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\Banda_Sonora  $banda_Sonora
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Banda_Sonora::deleted($id);
        return response()->json(['delete' => 'correct'], 200);
    }
}
