<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Http\Requests\UserRequest;
use SeriesYPeliculas\User;
use Illuminate\Http\Request;
use JWTAuth;
use jeremykenedy\LaravelRoles\Models\Role;


class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $u = JWTAuth::parseToken()->authenticate();
        if ($u->isAdmin()) {
            $users = User::paginate(10);
            return response()->json($users, 200);
        }
        return response()->json(['error' => 'You don\'t have permisions.'], 401);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        $role = Role::where('name', '=', 'User')->first();
        $user->attachRole($role);
        $token = JWTAuth::fromUser($user);

        return response()->json(['token' => $token], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $u = JWTAuth::parseToken()->authenticate();
        if ($u->id == $id || $u->isAdmin()) {
            $user = User::find($id);
            return response()->json($user, 200);
        }
        return response()->json(['error' => 'You don\'t have permissions.'], 401);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $u = JWTAuth::parseToken()->authenticate();
        if ($u->id == $id || $u->isAdmin()) {
            $user = User::find($id);
            return response()->json($user, 200);
        }
        return response()->json(['error' => 'You don\'t have permissions.'], 401);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $u = JWTAuth::parseToken()->authenticate();
        if ($u->id == $id || $u->isAdmin()) {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(["update" => "correct"], 200);
        }
        return response()->json(['error' => 'You don\'t have permissions.'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $u = JWTAuth::parseToken()->authenticate();
        if ($u->id == $id || $u->isAdmin()) {
            User::deleted($id);
            return response()->json(["delete" => "correct"], 200);
        }
        return response()->json(['error' => 'You don\'t have permissions.'], 401);
    }

}
