<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Actor;
use Illuminate\Http\Request;
use JWTAuth;
use Exception;
use SeriesYPeliculas\Http\Requests\ActorRequest;

class ActorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actor = Actor::paginate(10);
        return response()->json($actor);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActorRequest $request)
    {
        Actor::create([
            'nombre' => $request->nombre,
            'pais' => $request->pais,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'fecha_defuncion' => $request->fecha_defuncion,
        ]);

        return request()->json(['create' => 'correct'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actor = Actor::find($id);
        return response()->json($actor, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function edit(Actor $actor)
    {
        $actor = Actor::find($id);
        return response()->json($actor, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function update(Actor $request, $id)
    {
        $actor = Actor::find($id);
        $actor->nombre = $request->nombre;
        $actor->pais = $request->pais;
        $actor->fecha_nacimiento = $request->fecha_nacimiento;
        $actor->fecha_defuncion = $request->fecha_defuncion;
        $actor->save();

        return response()->json(["update" => "correct"], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Actor::deleted($id);

        return response()->json(["delete" => "correct"], 200);
    }
}
