<?php

namespace SeriesYPeliculas\Http\Controllers;

use SeriesYPeliculas\Compositor;
use SeriesYPeliculas\Http\Requests\CompositorRequest;

class CompositorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $compositor = Compositor::paginate(10);
        return response()->json($compositor, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompositorRequest $request)
    {
        Compositor::create([
            'nombre' => $request->nombre,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'fecha_defuncion' => $request->fecha_defuncion
        ]);

        return response()->json(['create' => 'correct'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SeriesYPeliculas\Compositor  $compositor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $compositor = Compositor::find($id);
        return response()->json($compositor, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SeriesYPeliculas\Compositor  $compositor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $compositor = Compositor::find($id);
        return response()->json($compositor, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SeriesYPeliculas\Compositor  $compositor
     * @return \Illuminate\Http\Response
     */
    public function update(CompositorRequest $request, $id)
    {
        $compositor = Compositor::find($id);
        $compositor->nombre = $request->nombre;
        $compositor->fecha_nacimiento;
        $compositor->fecha_defuncion;
        $compositor->save();

        return response()->json(['update' => 'correct'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SeriesYPeliculas\Compositor  $compositor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Compositor::deleted($id);
        return response()->json(['delete' => 'correct'], 200);
    }
}
