<?php

namespace SeriesYPeliculas\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \SeriesYPeliculas\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \Barryvdh\Cors\HandleCors::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \SeriesYPeliculas\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            // \SeriesYPeliculas\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            // TODO 'throttle:60,1',
            // TODO 'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \SeriesYPeliculas\Http\Middleware\RedirectIfAuthenticated::class,
        // TODO 'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        // Middleware para tokens
        'jwt.auth' => Tymon\JWTAuth\Http\Middleware\Authenticate::class,
        'jwt.refresh' => Tymon\JWTAuth\Http\Middleware\RefreshToken::class,
        // Middleware para roles
        'role'          => \jeremykenedy\LaravelRoles\Middleware\VerifyRole::class,
        'permission'    => \jeremykenedy\LaravelRoles\Middleware\VerifyPermission::class,
        'level'         => \jeremykenedy\LaravelRoles\Middleware\VerifyLevel::class,
        // Middleware para llamadas
        'cors' => \Barryvdh\Cors\HandleCors::class,
    ];
}
