<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Temporada extends Model
{
    protected $table = 'temporadas';

    public function serie(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\Serie');
    }

    public  function capitulos(): HasMany
    {
        return $this->hasMany('SeriesYPeliculas\Capitulo');
    }
}
