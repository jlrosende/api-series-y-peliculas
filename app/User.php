<?php

namespace SeriesYPeliculas;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use jeremykenedy\LaravelRoles\Models\Role;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    use HasRoleAndPermission;

    protected $table = 'users';
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['name', 'email', 'password'];
    protected $guarded = ['password', 'email'];
    protected $hidden = ['password', 'remember_token'];

    public function perfil(): HasOne
    {
        return $this->hasOne('SeriesYPeliculas\Perfil');
    }

    public function permissions(): BelongsToMany
    {
        return $this->userPermissions();
    }

    protected static function boot()
    {
        parent::boot();

        static::created((function ($user) {
            $role = Role::where('name', '=', 'User')->first();
            $user->attachRole($role);
        }));

        static::deleting(function ($user) {
            $user->perfil()->delete();
        });

        static::restoring(function ($user) {
            $user->perfil()->withTrashed()->restore();
        });
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
