<?php

namespace SeriesYPeliculas\Exceptions;

use Closure;
use GraphQL\Error\Error;
use Nuwave\Lighthouse\Execution\ErrorHandler;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;

class CustomErrorHandler implements ErrorHandler
{
    /**
     * Handle Exceptions that implement Nuwave\Lighthouse\Exceptions\RendersErrorsExtensions
     * and add extra content from them to the 'extensions' key of the Error that is rendered
     * to the User.
     *
     * @param  \GraphQL\Error\Error  $error
     * @param  \Closure  $next
     * @return array
     */
    public static function handle(Error $error, Closure $next): array
    {
        $underlyingException = $error->getPrevious();

        if ($underlyingException instanceof UnauthorizedHttpException) {

            $preException = $underlyingException->getPrevious();

            if ($preException instanceof TokenExpiredException) {
                $error = new Error(
                    $error->message,
                    $error->nodes,
                    $error->getSource(),
                    $error->getPositions(),
                    $error->getPath(),
                    null,
                    ['category' => 'EXPIRED_TOKEN']
                );
            } else if ($preException instanceof TokenInvalidException) {
                $error = new Error(
                    $error->message,
                    $error->nodes,
                    $error->getSource(),
                    $error->getPositions(),
                    $error->getPath(),
                    null,
                    ['category' => 'INVALID_TOKEN']
                );
            } else if ($preException instanceof TokenBlacklistedException) {
                $error = new Error(
                    $error->message,
                    $error->nodes,
                    $error->getSource(),
                    $error->getPositions(),
                    $error->getPath(),
                    null,
                    ['category' => 'BLAKLISTED_TOKEN']
                );
            }

            if ($error->getMessage() === 'Token not provided') {
                $error = new Error(
                    $error->message,
                    $error->nodes,
                    $error->getSource(),
                    $error->getPositions(),
                    $error->getPath(),
                    null,
                    ['category' => 'TOKEN_NOT_PROVIDED']
                );
            }
        }
        return $next($error);
    }
}
