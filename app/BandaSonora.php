<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BandaSonora extends Model
{
    protected $table = 'bandas_sonoras';
    protected $fillable = ['nombre'];

    public function pelicula(): HasOne
    {
        return $this->hasOne('SeriesYPeliculas\Pelicula');
    }

    public function compositor(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\Compositor');
    }

    public function canciones(): HasMany
    {
        return $this->hasMany('SeriesYPeliculas\Cancion');
    }
}
