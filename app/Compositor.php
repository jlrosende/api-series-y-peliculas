<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Compositor extends Model
{
    protected $table = 'compositores';

    public function bandas_sonoras(): HasMany
    {
        return $this->hasMany('SeriesYPeliculas\BandaSonora');
    }

    public function persona(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\Persona');
    }

    public function premios(): MorphToMany
    {
        return $this->morphToMany('SeriesYPeliculas\Premio', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function festivales(): MorphToMany
    {
        return $this->morphToMany('SeriesYPeliculas\Festival', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }
}
