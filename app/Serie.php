<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Serie extends Model
{
    protected $table = 'series';

    public function paises()
    {
        return $this->belongsToMany('SeriesYPeliculas\Pais', 'pais_serie');
    }

    public function generos(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Genero');
    }

    public function actores(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Actor');
    }

    public function directores(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Director');
    }

    public function guionistas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Guionista');
    }

    public function perfiles(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Perfil', 'perfil_serie')->withPivot('estado');
    }

    public function productores(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Productor', 'serie_productor');
    }

    public function temporadas(): HasMany
    {
        return $this->hasMany('SeriesYPeliculas\Temporada');
    }

    public function banda_sonora(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\BandaSonora');
    }

    public function plataformas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Plataforma', 'plataforma_serie')->withPivot(['url', 'inicio_disponible', 'fin_disponible']);
    }

    public function premios(): MorphToMany
    {
        return $this->morphToMany('SeriesYPeliculas\Premio', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function festivales(): MorphToMany
    {
        return $this->morphToMany('SeriesYPeliculas\Festival', 'premiado')->withPivot(['anio', 'edicion', 'estado', 'premio_id', 'festival_id']);
    }

    public function cartel($serie)
    {
        if ($serie->cartel)
            return Storage::disk('public')->url($serie->cartel);
        return null;
    }
}
