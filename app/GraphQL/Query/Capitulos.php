<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 29/8/17
 * Time: 18:51
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Capitulo;


class Capitulos
{
    protected $attributes = [
        'name' => 'capitulos',
        'description' => 'A query of capitulos'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Capitulo'));
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::id()
            ],
            'nombre' => [
                'name' => 'nombre',
                'type' => Type::string()
            ]
        ];
    }


    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Capitulo::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Capitulo::where('nombre', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else {
            return Capitulo::all();
        }
    }
}
