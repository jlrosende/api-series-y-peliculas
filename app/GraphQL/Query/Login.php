<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 3/2/18
 * Time: 13:01
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use Nuwave\Lighthouse\Exceptions\AuthenticationException as AuthenticationException;

class Login
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $credentials = [
            "email" => $args['email'],
            "password" => $args['password']
        ];

        if (!$token = auth('api')->attempt($credentials)) {
            throw new AuthenticationException("Invalid credentials");
        }

        return [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ];
    }
}
