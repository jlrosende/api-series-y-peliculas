<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 29/8/17
 * Time: 18:51
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\Pelicula;

class Peliculas
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        if (isset($args['id'])) {
            return Pelicula::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Pelicula::where('nombre', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else if (isset($args['anio'])) {
            return Pelicula::where('anio', $args['anio'])->get();
        } else {
            return Pelicula::all();
        }
    }
}
