<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 1/2/18
 * Time: 9:40
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;





use SeriesYPeliculas\Guionista;

class Guionistas
{

    protected $attributes = [
        'name' => 'guionistas',
        'description' => 'A query of guionistas'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Guionista'));
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::id()
            ]
        ];
    }

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Guionista::where('id', $args['id'])->get();
        } else {
            return Guionista::all();
        }
    }
}
