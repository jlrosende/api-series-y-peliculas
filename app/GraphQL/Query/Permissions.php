<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 1/2/18
 * Time: 10:23
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;





use jeremykenedy\LaravelRoles\Models\Permission;

class Permissions
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Permission::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Permission::where('name', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else if (isset($args['slug'])) {
            return Permission::where('slug', 'LIKE', '%' . $args['slug'] . '%')->get();
        } else if (isset($args['descripcion'])) {
            return Permission::where('description', 'LIKE', '%' . $args['descripcion'] . '%')->get();
        } else if (isset($args['model'])) {
            return Permission::where('model', 'LIKE', '%' . $args['model'] . '%')->get();
        } else {
            return Permission::all();
        }
    }
}
