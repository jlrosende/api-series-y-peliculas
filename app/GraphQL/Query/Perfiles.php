<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 1/2/18
 * Time: 10:02
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Perfil;

class Perfiles
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        if (isset($args['id'])) {
            return Perfil::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Perfil::where('nombre', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else if (isset($args['apellidos'])) {
            return Perfil::where('apellidos', 'LIKE', '%' . $args['apellidos'] . '%')->get();
        } else if (isset($args['telefono'])) {
            return Perfil::where('telefono', 'LIKE', '%' . $args['telefono'] . '%')->get();
        } else if (isset($args['cpostal'])) {
            return Perfil::where('cpostal', 'LIKE', '%' . $args['cpostal'] . '%')->get();
        } else if (isset($args['direccion'])) {
            return Perfil::where('direccion', 'LIKE', '%' . $args['direccion'] . '%')->get();
        } else {
            return Perfil::all();
        }
    }
}
