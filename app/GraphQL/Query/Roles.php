<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 1/2/18
 * Time: 10:40
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;





use jeremykenedy\LaravelRoles\Models\Role;

class Roles
{



    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Role::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Role::where('name', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else if (isset($args['slug'])) {
            return Role::where('slug', 'LIKE', '%' . $args['slug'] . '%')->get();
        } else if (isset($args['descripcion'])) {
            return Role::where('description', 'LIKE', '%' . $args['descripcion'] . '%')->get();
        } else if (isset($args['level'])) {
            return Role::where('level', $args['model'])->get();
        } else {
            return Role::all();
        }
    }
}
