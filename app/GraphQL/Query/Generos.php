<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:26
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;





use SeriesYPeliculas\Genero;

class Generos
{

    protected $attributes = [
        'name' => 'generos',
        'description' => 'A query of generos'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Genero'));
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::id()
            ],
            'nombre' => [
                'name' => 'nombre',
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Genero::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Genero::where('nombre', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else {
            return Genero::all();
        }
    }
}
