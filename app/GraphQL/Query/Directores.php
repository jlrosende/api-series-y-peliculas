<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:26
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;





use SeriesYPeliculas\Director;
use SeriesYPeliculas\Persona;

class Directores
{

    protected $attributes = [
        'name' => 'directores',
        'description' => 'A query of directores'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Director'));
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::id()
            ]
            //Todo Buscar por nombre
        ];
    }

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Director::where('id', $args['id'])->get();
        } else {
            return Director::all();
        }
    }
}
