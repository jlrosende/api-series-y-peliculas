<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 1/2/18
 * Time: 10:37
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\Productor;

class Productores
{

    protected $attributes = [
        'name' => 'productores',
        'description' => 'A query of productores'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Productor'));
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::id()
            ]
        ];
    }

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Productor::where('id', $args['id'])->get();
        } else {
            return Productor::all();
        }
    }
}
