<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:26
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;





use SeriesYPeliculas\Compositor;

class Compositores
{

    protected $attributes = [
        'name' => 'compositores',
        'description' => 'A query of compositores'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Compositor'));
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::id()
            ],
            'nombre' => [
                'name' => 'nombre',
                'type' => Type::string()
            ],
            'fecha_nacimiento' => [
                'name' => 'fecha_nacimiento',
                'type' => Type::string()
            ],
            'fecha_defuncion' => [
                'name' => 'fecha_defuncion',
                'type' => Type::string()
            ],

        ];
    }

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Compositor::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Compositor::where('email', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else {
            return Compositor::all();
        }
    }
}
