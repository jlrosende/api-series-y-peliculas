<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 1/2/18
 * Time: 9:50
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Pais;

class Paises
{

    protected $attributes = [
        'name' => 'paises',
        'description' => 'A query of paises'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Pais'));
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::id()
            ],
            'nombre' => [
                'name' => 'nombre',
                'type' => Type::string()
            ],
            'siglas' => [
                'name' => 'siglas',
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Pais::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Pais::where('nombre', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else if (isset($args['siglas'])) {
            return Pais::where('siglas', 'LIKE', '%' . $args['siglas'] . '%')->get();
        } else {
            return Pais::all();
        }
    }
}
