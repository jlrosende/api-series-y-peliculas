<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 1/2/18
 * Time: 10:32
 */


namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Persona;

class Personas
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Persona::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Persona::where('nombre', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else {
            return Persona::all();
        }
    }
}
