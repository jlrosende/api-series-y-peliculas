<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 1/2/18
 * Time: 0:50
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\BandaSonora;


class BandasSonoras
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)

    {
        if (isset($args['id'])) {
            return BandaSonora::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return BandaSonora::where('nombre', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else {
            return BandaSonora::all();
        }
    }
}
