<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:26
 */

namespace SeriesYPeliculas\GraphQL\Query;

use SeriesYPeliculas\User;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

class Users
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        if (isset($args['id'])) {
            return [User::find($args['id'])];
        }

        if (isset($args['email'])) {
            return User::where('email', 'LIKE', '%' . $args['email'] . '%')->get();
        }
        return User::all();
    }
}
