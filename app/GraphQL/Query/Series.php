<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 24/10/17
 * Time: 12:16
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Serie;

class Series
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Serie::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Serie::where('nombre', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else if (isset($args['anio'])) {
            return Serie::where('anio', $args['anio'])->get();
        } else {
            return Serie::all();
        }
    }
}
