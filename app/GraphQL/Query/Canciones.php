<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 1/2/18
 * Time: 0:50
 */

namespace SeriesYPeliculas\GraphQL\Query;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Cancion;


class Canciones
{
    protected $attributes = [
        'name' => 'canciones',
        'description' => 'A query of canciones'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Cancion'));
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::id()
            ],
            'nombre' => [
                'name' => 'nombre',
                'type' => Type::string()
            ]
        ];
    }


    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        if (isset($args['id'])) {
            return Cancion::where('id', $args['id'])->get();
        } else if (isset($args['nombre'])) {
            return Cancion::where('nombre', 'LIKE', '%' . $args['nombre'] . '%')->get();
        } else {
            return Cancion::all();
        }
    }
}
