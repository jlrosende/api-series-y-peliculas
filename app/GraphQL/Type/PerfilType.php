<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;


use Illuminate\Support\Facades\Storage;

use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Perfil;

class PerfilType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Perfil',
        'description' => 'Perfil del usuario',
        'model' => Perfil::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'Id del perfil'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'Nombre del perfil',
            ],
            'apellidos' => [
                'type' => Type::string(),
                'description' => 'Apellidos del perfil',
            ],
            'avatar' => [
                'type' => Type::string(),
                'description' => 'Avatar del perfil',
            ],
            'telefono' => [
                'type' => Type::string(),
                'description' => 'Telefono del perfil',
            ],
            'cpostal' => [
                'type' => Type::string(),
                'description' => 'Codigo postal del perfil',
            ],
            'direccion' => [
                'type' => Type::string(),
                'description' => 'Direccion del perfil',
            ],
            'user' => [
                'type' => GraphQL::type('User'),
                'description' => 'user al que pertenece el perfil',
            ],
            'n_peliculas' => [
                'type' => Type::int(),
                'description' => 'Numero de peliculas pendientes o vistas',
            ],
            'peliculas' => [
                'type' => Type::listOf(GraphQL::type('Pelicula')),
                'description' => 'Lista de peliculas',
            ],
            'n_series' => [
                'type' => Type::int(),
                'description' => 'Numero de series pendientes o vistas',
            ],
            'series' => [
                'type' => Type::listOf(GraphQL::type('Serie')),
                'description' => 'Lista de series',
            ]
        ];
    }

    public function resolvePeliculasField($root, $args)
    {
        foreach ($root->peliculas as $key => $pelicula) {
            $pelicula['estado'] = $pelicula->pivot->estado;
        }
        return $root->peliculas;
    }

    public function resolveSeriesField($root, $args)
    {
        foreach ($root->series as $key => $serie) {
            $serie['estado'] = $serie->pivot->estado;
        }
        return $root->series;
    }

    public function resolveAvatarField($root, $args)
    {
        if ($root->avatar != null) {
            return "http://api.peliculas" . Storage::url($root->avatar);
        }
        return null;
    }

    public function resolveNPeliculasField($root, $args)
    {
        return $root->peliculas->count();
    }

    public function resolveNSeriesField($root, $args)
    {
        return $root->series->count();
    }
}
