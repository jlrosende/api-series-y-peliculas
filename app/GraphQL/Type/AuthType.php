<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 3/2/18
 * Time: 19:22
 */

namespace SeriesYPeliculas\GraphQL\Type;

use GraphQL\Error\Error;


use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\GraphQL\Privacy\AdminPrivacy;
use SeriesYPeliculas\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Token',
        'description' => 'Token'
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'token' => [
                'type' => Type::string(),
                'description' => 'The token of the user'
            ]
        ];
    }

    // If you want to resolve the field yourself, you can declare a method
    // with the following format resolve[FIELD_NAME]Field()
    protected function resolveTokenField($root, $args)
    {
        return $root;
    }
}
