<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use jeremykenedy\LaravelRoles\Models\Role;

class RoleType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Role',
        'description' => 'Role',
        'model' => Role::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'The id of the role'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'Nombre del role'
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'Slug del role'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'Descripcion del role'
            ],
            'level' => [
                'type' => Type::int(),
                'description' => 'Nivel de acceso del role'
            ],
            'permissions' => [
                'type' => Type::listOf(GraphQL::type('Permission')),
                'description' => 'Permisos del role'
            ],
            'users' => [
                'type' => Type::listOf(GraphQL::type('User')),
                'description' => 'Usuarios que tienen el role'
            ],

        ];
    }
}
