<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Capitulo;

class CapituloType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Capitulo',
        'description' => 'Un capitulo',
        'model' => Capitulo::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'Id del capitulo'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'Nombre del capitulo'
            ],
            'sinopsis' => [
                'type' => Type::string(),
                'description' => 'Sinopsis del capitulo'
            ],
            'duracion' => [
                'type' => Type::string(),
                'description' => 'Duracion del capitulo'
            ],
            'temporada' => [
                'type' => GraphQL::type('Temporada'),
                'description' => 'Temporada del capitulo'
            ]
        ];
    }
}
