<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Temporada;

class TemporadaType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Temporada',
        'description' => 'Temporada',
        'model' => Temporada::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'The id of the user'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'Nombre del Actor'
            ],
            'n_capitulos' => [
                'type' => Type::int(),
                'description' => 'Numero de capitulos de la temporada',
                'selectable' => false
            ],
            'capitulos' => [
                'type' => Type::listOf(GraphQL::type('Capitulo')),
                'description' => 'Capitulos de la temporada',
            ],
            'serie' => [
                'type' => GraphQL::type('Serie'),
                'description' => 'Serie de la temporada',
            ],
        ];
    }

    protected function resolveNCapitulosField($root, $args)
    {
        return $root->capitulos->count();
    }
}
