<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Cancion;

class CancionType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Cancion',
        'description' => 'Cancion',
        'model' => Cancion::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'The id of the user'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'Nombre de la cancion'
            ],
            'duracion' => [
                'type' => Type::string(),
                'description' => 'Duracion de la cancion'
            ],
            'banda_sonora' => [
                'type' => GraphQL::type('BandaSonora'),
                'description' => 'Cancion de la banda sonora'
            ],
        ];
    }
}
