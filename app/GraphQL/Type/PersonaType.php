<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;


use Illuminate\Support\Facades\Storage;

use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Persona;

class PersonaType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Persona',
        'description' => 'Persona',
        'model' => Persona::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'Id de la persona'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'Nombre de la persona'
            ],
            'foto' => [
                'type' => Type::string(),
                'description' => 'Foto de la persona'
            ],
            'fecha_nacimiento' => [
                'type' => Type::string(),
                'description' => 'Fecha de nacimiento de la persona'
            ],
            'fecha_defuncion' => [
                'type' => Type::string(),
                'description' => 'Fecha de defuncion de la persona'
            ],
            'paises' => [
                'type' => Type::listOf(GraphQL::type('Pais')),
                'description' => 'Pais de procedencia de la persona'
            ],
            'director' => [
                'type' => GraphQL::type('Director'),
                'description' => 'Director Persona'
            ],
            'guionista' => [
                'type' => GraphQL::type('Guionista'),
                'description' => 'Guionista persona'
            ],
            'actor' => [
                'type' => GraphQL::type('Actor'),
                'description' => 'Actor persona'
            ],
            'compositor' => [
                'type' => GraphQL::type('Compositor'),
                'description' => 'Compositor persona'
            ],
            'productor' => [
                'type' => GraphQL::type('Productor'),
                'description' => 'Productor persona'
            ],
        ];
    }

    public function resolveFotoField($root, $args)
    {
        if ($root->foto != null) {
            return "http://api.peliculas" . Storage::url($root->foto);
        }
        return null;
    }
}
