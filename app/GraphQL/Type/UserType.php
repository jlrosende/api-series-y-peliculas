<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\GraphQL\Privacy\AdminPrivacy;
use SeriesYPeliculas\User;
use JWTAuth;

class UserType extends GraphQLType
{

    protected $attributes = [
        'name' => 'User',
        'description' => 'A user',
        'model' => User::class
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'The id of the user'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of the user',
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'Email del usuario'
            ],
            'perfil' => [
                'type' => GraphQL::type('Perfil'),
                'description' => 'Perfil del usuario'
            ],
            'roles' => [
                'type' => Type::listOf(GraphQL::type('Role')),
                'description' => 'Role del usuario',
                'privacy' =>  function (array $args) {
                    $user = JWTAuth::parseToken()->authenticate();
                    return $user->isAdmin();
                }
            ],
            'permissions' => [
                'type' => Type::listOf(GraphQL::type('Permission')),
                'description' => 'Permisos del usuario',
                'privacy' =>  function (array $args) {
                    $user = JWTAuth::parseToken()->authenticate();
                    return $user->isAdmin();
                }
            ],
            'is_admin' => [
                'type' => Type::boolean(),
                'description' => 'Es administrador',
                'privacy' =>  function (array $args) {
                    $user = JWTAuth::parseToken()->authenticate();
                    return $user->isAdmin();
                }
            ],
            'created_at'  => [
                'type' => Type::string(),
                'description' => 'Creation Date',
                'privacy' =>  function (array $args) {
                    $user = JWTAuth::parseToken()->authenticate();
                    return $user->isAdmin();
                }
            ],
            'updated_at'  => [
                'type' => Type::string(),
                'description' => 'Update Date',
                'privacy' =>  function (array $args) {
                    $user = JWTAuth::parseToken()->authenticate();
                    return $user->isAdmin();
                }
            ]
        ];
    }


    // If you want to resolve the field yourself, you can declare a method
    // with the following format resolve[FIELD_NAME]Field()
    protected function resolveEmailField($root, $args)
    {
        return strtolower($root->email);
    }

    public function resolveCreatedAtField($root, $args)
    {
        return date(DATE_ATOM, strtotime($root->created_at));
    }

    public function resolveUpdatedAtField($root, $args)
    {
        return date(DATE_ATOM, strtotime($root->updated_at));
    }

    public function resolveIsAdminField($root, $args)
    {
        return $root->isAdmin();
    }
}
