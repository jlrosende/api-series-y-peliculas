<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;


use Illuminate\Support\Facades\Storage;

use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Serie;

class SerieType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Serie',
        'description' => 'Una serie',
        'model' => Serie::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'The id of the serie'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'The name of the serie'
            ],
            'anio' => [
                'type' => Type::string(),
                'description' => 'The year of the serie'
            ],
            'sinopsis' => [
                'type' => Type::string(),
                'description' => 'The sinopsis of the serie'
            ],
            'cartel' => [
                'type' => Type::string(),
                'description' => 'The poster of the serie'
            ],
            'paises' => [
                'type' => Type::listOf(GraphQL::type('Pais')),
                'description' => 'Paises de la serie'
            ],
            'generos' => [
                'type' => Type::listOf(GraphQL::type('Genero')),
                'description' => 'Generos de la serie'
            ],
            'directores' => [
                'type' => Type::listOf(GraphQL::type('Director')),
                'description' => 'Directores de la serie'
            ],
            'actores' => [
                'type' => Type::listOf(GraphQL::type('Actor')),
                'description' => 'Actores de la serie'
            ],
            'guionistas' => [
                'type' => Type::listOf(GraphQL::type('Guionista')),
                'description' => 'Guionistas de la serie'
            ],
            'productores' => [
                'type' => Type::listOf(GraphQL::type('Productor')),
                'description' => 'Productores de la serie'
            ],
            'banda_sonora' => [
                'type' => GraphQL::type('BandaSonora'),
                'description' => 'Banda sonora de la pelicula'
            ],
            'temporadas' => [
                'type' => Type::listOf(GraphQL::type('Temporada')),
                'description' => 'Temporadas de la serie'
            ],
            'n_temporadas' => [
                'type' => Type::int(),
                'description' => 'Numero de temporadas de la serie',
                'selectable' => false
            ],
            'estado'  => [
                'type' => Type::string(),
                'description' => 'Estado'
            ],
        ];
    }

    public function resolveCartelField($root, $args)
    {
        if ($root->cartel != null) {
            return "http://api.peliculas" . Storage::url($root->cartel);
        }
        return null;
    }

    protected function resolveNTemporadasField($root, $args)
    {
        return $root->temporadas->count();
    }
}
