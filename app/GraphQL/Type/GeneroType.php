<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Genero;

class GeneroType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Genero',
        'description' => 'Genero',
        'model' => Genero::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'Id del genero'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'Nombre del genero'
            ],
            'peliculas' => [
                'type' => Type::listOf(GraphQL::type('Pelicula')),
                'description' => 'Lista de peliculas',
            ],
            'series' => [
                'type' => Type::listOf(GraphQL::type('Serie')),
                'description' => 'Lista de series',
            ]
        ];
    }
}
