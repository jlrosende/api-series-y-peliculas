<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;


use Illuminate\Support\Facades\Storage;

use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Pelicula;
use JWTAuth;

class PeliculaType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Pelicula',
        'description' => 'Una pelicula',
        'model' => Pelicula::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'The id of the user'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'The name of the film'
            ],
            'anio' => [
                'type' => Type::string(),
                'description' => 'The year of the film'
            ],
            'sinopsis' => [
                'type' => Type::string(),
                'description' => 'The sinopsis of the film'
            ],
            'duracion' => [
                'type' => Type::string(),
                'description' => 'The duration of the film'
            ],
            'cartel' => [
                'type' => Type::string(),
                'description' => 'The poster of the film'
            ],
            'paises' => [
                'type' => Type::listOf(GraphQL::type('Pais')),
                'description' => 'Paises de la pelicula'
            ],
            'generos' => [
                'type' => Type::listOf(GraphQL::type('Genero')),
                'description' => 'Generos de la pelicula'
            ],
            'directores' => [
                'type' => Type::listOf(GraphQL::type('Director')),
                'description' => 'Directores de la pelicula'
            ],
            'actores' => [
                'type' => Type::listOf(GraphQL::type('Actor')),
                'description' => 'Actores de la pelicula'
            ],
            'guionistas' => [
                'type' => Type::listOf(GraphQL::type('Guionista')),
                'description' => 'Guionistas de la pelicula'
            ],
            'productores' => [
                'type' => Type::listOf(GraphQL::type('Productor')),
                'description' => 'Productores de la pelicula'
            ],
            'banda_sonora' => [
                'type' => GraphQL::type('BandaSonora'),
                'description' => 'Banda sonora de la pelicula'
            ],
            'estado'  => [
                'type' => Type::string(),
                'description' => 'Estado de visualizacion del usuario'
            ],
            'created_at'  => [
                'type' => Type::string(),
                'description' => 'Creation Date',
                'privacy' => function (array $args) {
                    $user = JWTAuth::parseToken()->authenticate();
                    return $user->isAdmin();
                }
            ],
            'updated_at'  => [
                'type' => Type::string(),
                'description' => 'Update Date',
                'privacy' => function (array $args) {
                    $user = JWTAuth::parseToken()->authenticate();
                    return $user->isAdmin();
                }
            ]
        ];
    }

    public function resolveCartelField($root, $args)
    {
        if ($root->cartel != null) {
            return 'data:image/' . pathinfo($root->cartel, PATHINFO_EXTENSION) . ';base64,' . base64_encode(Storage::disk('public')->get($root->cartel));
            // return "http://api.peliculas".Storage::url($root->cartel);
        }
        return null;
    }

    public function resolveCreatedAtField($root, $args)
    {
        if (!$root->created_at) {
            return null;
        }
        return date(DATE_ATOM, strtotime($root->created_at));
    }

    public function resolveUpdatedAtField($root, $args)
    {
        if (!$root->updated_at) {
            return null;
        }
        return date(DATE_ATOM, strtotime($root->updated_at));
    }
}
