<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Compositor;

class CompositorType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Compositor',
        'description' => 'Compositor',
        'model' => Compositor::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'Id del compositor'
            ],
            'persona' => [
                'type' => GraphQL::type('Persona'),
                'description' => 'Info del compositor'
            ],
            'bandas_sonoras' => [
                'type' => Type::listOf(GraphQL::type('BandaSonora')),
                'description' => 'Bandas sonoras creadas por el compositor'
            ],
            'n_bandas_sonoras' => [
                'type' => Type::int(),
                'description' => 'Numero de Bandas sonoras creadas por el compositor'
            ],
        ];
    }

    public function resolveNBandasSonorasField($root, $args)
    {
        return $root->bandas_sonoras->count();
    }
}
