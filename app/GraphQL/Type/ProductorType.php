<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Productor;

class ProductorType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Productor',
        'description' => 'Productor',
        'model' => Productor::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'Id del productor'
            ],
            'persona' => [
                'type' => GraphQL::type('Persona'),
                'description' => 'Info del productor'
            ],
            'peliculas' => [
                'type' => Type::listOf(GraphQL::type('Pelicula')),
                'description' => 'Peliculas del productor'
            ],
            'series' => [
                'type' => Type::listOf(GraphQL::type('Serie')),
                'description' => 'Series del productor'
            ],
            'n_peliculas' => [
                'type' => Type::int(),
                'description' => 'Numero de peliculas en las que participa el actor'
            ],
            'n_series' => [
                'type' => Type::int(),
                'description' => 'Numero de series en las que participa el actor'
            ],
        ];
    }

    public function resolveNPeliculasField($root, $args)
    {
        return $root->peliculas->count();
    }

    public function resolveNSeriesField($root, $args)
    {
        return $root->series->count();
    }
}
