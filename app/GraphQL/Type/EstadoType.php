<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Pelicula;


class EstadoType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Estado',
        'description' => 'Una estado',
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'estado'  => [
                'type' => Type::string(),
                'description' => 'The duration of the film'
            ],
        ];
    }
}
