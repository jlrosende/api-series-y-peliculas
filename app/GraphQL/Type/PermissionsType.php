<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use jeremykenedy\LaravelRoles\Models\Permission;

class PermissionsType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Permissions',
        'description' => 'Permissions',
        'model' => Permission::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'The id of the permission'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'name of the permission'
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'slug of the permission'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'desciption of the permission'
            ],
            'model' => [
                'type' => Type::string(),
                'description' => 'model of the permission'
            ],
            'roles' => [
                'type' => Type::listOf(GraphQL::type('Role')),
                'description' => 'role of the permission'
            ],
            'users' => [
                'type' => Type::listOf(GraphQL::type('User')),
                'description' => 'Usuarios que tienen el role'
            ],
        ];
    }
}
