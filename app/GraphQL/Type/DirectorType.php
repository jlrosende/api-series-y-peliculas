<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\Director;

class DirectorType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Director',
        'description' => 'Director',
        'model' => Director::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'Id del director'
            ],
            'persona' => [
                'type' => GraphQL::type('Persona'),
                'description' => 'Info del compositor'
            ],
            'peliculas' => [
                'type' => Type::listOf(GraphQL::type('Pelicula')),
                'description' => 'Peliculas del director'
            ],
            'series' => [
                'type' => Type::listOf(GraphQL::type('Serie')),
                'description' => 'Series del guionista'
            ],
            'n_peliculas' => [
                'type' => Type::int(),
                'description' => 'Numero de peliculas dirigidas por este director'
            ],
            'n_series' => [
                'type' => Type::int(),
                'description' => 'Numero de series dirigidas por este director'
            ],
        ];
    }

    public function resolveNPeliculasField($root, $args)
    {
        return $root->peliculas->count();
    }

    public function resolveNSeriesField($root, $args)
    {
        return $root->series->count();
    }
}
