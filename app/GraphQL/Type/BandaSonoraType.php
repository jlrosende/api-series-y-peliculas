<?php
/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 28/8/17
 * Time: 17:20
 */
namespace SeriesYPeliculas\GraphQL\Type;



use Rebing\GraphQL\Support\Type as GraphQLType;
use SeriesYPeliculas\BandaSonora;

class BandaSonoraType extends GraphQLType
{

    protected $attributes = [
        'name' => 'BandaSonora',
        'description' => 'Banda sonora',
        'model' => BandaSonora::class,
    ];

    /*
       * Uncomment following line to make the type input object.
       * http://graphql.org/learn/schema/#input-types
       */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'The id of the user'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'The name of the banda sonora'
            ],
            'pelicula' => [
                'type' => GraphQL::type('Pelicula'),
                'description' => 'Pelicula de la banda sonora'
            ],
            'compositor' => [
                'type' => GraphQL::type('Compositor'),
                'description' => 'Compositor de la banda sonora'
            ],
            'canciones' => [
                'type' => Type::listOf(GraphQL::type('Cancion')),
                'description' => 'Canciones de la banda sonora'
            ],
            'n_canciones' => [
                'type' => Type::int(),
                'description' => 'Numero de canciones de la banda sonora'
            ],
        ];
    }

    public function resolveNCancionesField($root, $args)
    {
        return $root->canciones->count();
    }
}
