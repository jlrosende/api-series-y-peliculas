<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 6/2/18
 * Time: 0:41
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;



use SeriesYPeliculas\Actor;


class DeleteActor
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $actor = Actor::findOrFail($args['id']);
        if ($actor->delete()) {
            return $actor;
        }
        return null;
    }
}
