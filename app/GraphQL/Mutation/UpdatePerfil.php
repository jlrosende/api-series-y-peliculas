<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 17:28
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use Illuminate\Support\Facades\Storage;

use SeriesYPeliculas\Perfil;
use SeriesYPeliculas\User;

class UpdatePerfil
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $perfil = Perfil::find($args['id']);

        $user = User::find($args['user']);
        $perfil->user()->associate($user);

        $perfil->nombre = $args['nombre'];

        $perfil->apellidos = $args['apellidos'];

        if (isset($args['avatar'])) {
            if ($perfil->avatar) {
                Storage::disk('public')->delete($perfil->avatar);
            }
            $perfil->avatar = $args['avatar']->store(
                'avatares/' . $args['user'],
                'public'
            );
        }

        if (isset($args['telefono'])) {
            $perfil->telefono = $args['telefono'];
        }

        if (isset($args['cpostal'])) {
            $perfil->cpostal = $args['cpostal'];
        }

        if (isset($args['direccion'])) {
            $perfil->direccion = $args['direccion'];
        }

        if (isset($args['imagen'])) {
            $perfil->imagen = $args['imagen'];
        }

        $perfil->save();

        return $perfil;
    }
}
