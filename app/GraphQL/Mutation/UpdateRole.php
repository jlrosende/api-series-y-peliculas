<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 14/2/18
 * Time: 13:23
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use jeremykenedy\LaravelRoles\Models\Role;

class UpdateRole
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {

        $role = Role::find($args['id']);

        $role->name = $args['name'];

        $role->slug = $args['slug'];

        if (isset($args['description'])) {
            $role->description = $args['description'];
        }

        $role->level = $args['level'];

        if (isset($args['permissions'])) {
            $role->permissions()->sync($args['permissions']);
        }

        $role->save();

        return $role;
    }
}
