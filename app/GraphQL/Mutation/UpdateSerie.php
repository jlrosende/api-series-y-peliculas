<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 15/2/18
 * Time: 15:11
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use Illuminate\Support\Facades\Storage;

use SeriesYPeliculas\BandaSonora;
use SeriesYPeliculas\Serie;

class UpdateSerie
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {

        $serie = Serie::find($args['id']);

        if (isset($args['nombre'])) {
            $serie->nombre = $args['nombre'];
        }

        if (isset($args['anio'])) {
            $serie->anio = $args['anio'];
        }

        if (isset($args['sinopsis'])) {
            $serie->sinopsis = $args['sinopsis'];
        }

        if (isset($args['cartel'])) {
            if ($serie->cartel) {
                Storage::disk('public')->delete($serie->cartel);
            }
            $serie->cartel = $args['cartel']->store(
                'series/' . $args['id'],
                'public'
            );
        }

        if (isset($args['paises'])) {
            $serie->paises()->sync($args['paises']);
        }

        if (isset($args['generos'])) {
            $serie->generos()->sync($args['generos']);
        }

        if (isset($args['directores'])) {
            $serie->directores()->sync($args['directores']);
        }

        if (isset($args['actores'])) {
            $serie->actores()->sync($args['actores']);
        }

        if (isset($args['guionistas'])) {
            $serie->guionistas()->sync($args['guionistas']);
        }

        if (isset($args['productores'])) {
            $serie->productores()->sync($args['productores']);
        }

        if (isset($args['banda_sonora'])) {
            $banda_sonora = BandaSonora::findOrFail($args['banda_sonora']);
            $serie->banda_sonora()->associate($banda_sonora);
        }

        if (isset($args['plataformas'])) {
            $serie->plataformas()->sync($args['plataformas']);
        }

        $serie->save();

        return $serie;
    }
}
