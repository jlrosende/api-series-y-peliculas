<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 7/2/18
 * Time: 13:07
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use jeremykenedy\LaravelRoles\Models\Permission;

class DeletePermission
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {
        $permission = Permission::findOrFail($args['id']);
        if ($permission->delete()) {
            return $permission;
        }
        return null;
    }
}
