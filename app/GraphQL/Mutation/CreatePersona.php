<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 14/2/18
 * Time: 12:49
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use Illuminate\Support\Facades\Storage;



use SeriesYPeliculas\Pais;
use SeriesYPeliculas\Persona;

class CreatePersona
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $persona = new Persona;

        $persona->nombre = $args['nombre'];

        if (isset($args['foto'])) {
            $persona->avatar = $args['foto']->store(
                'personas/' . $args['id'],
                'public'
            );
        }

        if (isset($args['fecha_nacimiento'])) {
            $persona->fecha_nacimiento = $args['fecha_nacimiento'];
        }

        if (isset($args['fecha_defuncion'])) {
            $persona->fecha_defuncion = $args['fecha_defuncion'];
        }

        $persona->save();

        if (isset($args['paises'])) {
            foreach ($args['paises'] as $id) {
                $pais = Pais::find($id);
                $persona->paises()->attach($pais);
            }
        }

        $persona->save();

        return $persona;
    }
}
