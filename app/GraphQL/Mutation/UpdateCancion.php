<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 2:05
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\BandaSonora;
use SeriesYPeliculas\Cancion;


class UpdateCancion
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $cancion = Cancion::find($args['id']);

        $cancion->nombre = $args['nombre'];

        if (isset($args['duracion'])) {
            $cancion->duracion = date("H:i:s", strtotime($args['duracion']));
        }

        $banda_sonora = BandaSonora::find($args['banda_sonora']);
        $cancion->bandaSonora()->associate($banda_sonora);

        $cancion->save();

        return $cancion;
    }
}
