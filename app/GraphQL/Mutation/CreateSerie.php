<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 15/2/18
 * Time: 15:10
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;


use Illuminate\Support\Facades\Storage;


use SeriesYPeliculas\Actor;
use SeriesYPeliculas\BandaSonora;
use SeriesYPeliculas\Director;
use SeriesYPeliculas\Genero;
use SeriesYPeliculas\Guionista;
use SeriesYPeliculas\Pais;
use SeriesYPeliculas\Productor;
use SeriesYPeliculas\Serie;
use SeriesYPeliculas\Plataforma;

class CreateSerie
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $serie = new Serie;

        $serie->nombre = $args['nombre'];

        if (isset($args['anio'])) {
            $serie->anio = date($args['anio']);
        }

        if (isset($args['sinopsis'])) {
            $serie->sinopsis = $args['sinopsis'];
        }

        if (isset($args['cartel'])) {
            $serie->cartel = $args['cartel']->store(
                'series/' . $args['id'],
                'public'
            );
        }

        $serie->save();

        if (isset($args['paises'])) {
            foreach ($args['paises'] as $id) {
                $pais =  Pais::find($id);
                $serie->paises()->attach($pais->id);
            }
        }

        if (isset($args['generos'])) {
            foreach ($args['generos'] as $id) {
                $genero =  Genero::find($id);
                $serie->generos()->attach($genero->id);
            }
        }

        if (isset($args['directores'])) {
            foreach ($args['directores'] as $id) {
                $director =  Director::find($id);
                $serie->directores()->attach($director->id);
            }
        }

        if (isset($args['actores'])) {
            foreach ($args['actores'] as $id) {
                $actor =  Actor::find($id);
                $serie->actores()->attach($actor->id);
            }
        }

        if (isset($args['guionistas'])) {
            foreach ($args['guionistas'] as $id) {
                $guionista =  Guionista::find($id);
                $serie->guionistas()->attach($guionista->id);
            }
        }

        if (isset($args['productores'])) {
            foreach ($args['productores'] as $id) {
                $productor =  Productor::find($id);
                $serie->productores()->attach($productor->id);
            }
        }

        if (isset($args['plataformas'])) {
            foreach ($args['plataformas'] as $id) {
                $plataforma =  Plataforma::find($id);
                $serie->plataformas()->attach($plataforma->id);
            }
        }

        if (isset($args['banda_sonora'])) {
            $banda_sonora = BandaSonora::find($args['banda_sonora']);
            $serie->banda_sonora()->associate($banda_sonora);
        }

        $serie->save();

        return $serie;
    }
}
