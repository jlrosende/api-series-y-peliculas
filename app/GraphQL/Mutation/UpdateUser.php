<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 15/2/18
 * Time: 15:50
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\User;

class UpdateUser
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $user = User::find($args['id']);

        $user->name = $args['name'];
        $user->email = $args['email'];

        if (isset($args['new_password'])) {
            $user->password = bcrypt($args['new_password']);
        }

        if (isset($args['roles'])) {
            $user->roles()->sync($args['roles']);
        }

        if (isset($args['permissions'])) {
            $user->permissions()->sync($args['permissions']);
        }

        $user->save();

        return $user;
    }
}
