<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 7/2/18
 * Time: 12:51
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;



use SeriesYPeliculas\Compositor;


class DeleteCompositor
{



    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $compositor = Compositor::findOrFail($args['id']);
        if ($compositor->delete()) {
            return $compositor;
        }
        return null;
    }
}
