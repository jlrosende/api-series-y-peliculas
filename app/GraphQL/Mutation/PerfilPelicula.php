<?php

namespace SeriesYPeliculas\GraphQL\Mutation;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use SeriesYPeliculas\Perfil;

class PerfilPelicula
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $perfil = Perfil::findOrFail($args['perfil_id']);

        $peliculas = $args['peliculas'];

        if (isset($peliculas['connect'])) {
            $collection = collect($peliculas['connect']);
            $data = $collection->mapWithKeys(function ($item) {
                return [$item['pelicula_id'] => ['estado' => $item['estado']]];
            });
            $perfil->peliculas()->syncWithoutDetaching($data);
        } else if (isset($peliculas['disconnect'])) {
            $collection = collect($peliculas['disconnect']);
            $data = $collection->map(function ($item) {
                return $item['pelicula_id'];
            });
            $perfil->peliculas()->detach($data);
        }

        return $perfil;
    }
}
