<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 6/2/18
 * Time: 0:31
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Actor;
use SeriesYPeliculas\Persona;

class UpdateActor
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $actor = Actor::find($args['id']);

        $persona = Persona::find($args['persona']);
        $actor->persona()->associate($persona);

        if (isset($args['peliculas'])) {
            $actor->peliculas()->sync($args['peliculas']);
        }

        if (isset($args['series'])) {
            $actor->series()->sync($args['series']);
        }

        $actor->save();

        return $actor;
    }
}
