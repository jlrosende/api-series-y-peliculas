<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 1:59
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\BandaSonora;
use SeriesYPeliculas\Cancion;


class CreateCancion
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {

        $cancion = new Cancion;

        $cancion->nombre = $args['nombre'];

        if (isset($args['duracion'])) {
            $cancion->duracion = date("H:i:s", strtotime($args['duracion']));
        }

        $banda_sonora = BandaSonora::find($args['banda_sonora']);
        $cancion->bandaSonora()->associate($banda_sonora);

        $cancion->save();

        return $cancion;
    }
}
