<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 15/2/18
 * Time: 15:50
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use jeremykenedy\LaravelRoles\Models\Role;

use SeriesYPeliculas\User;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

class CreateUser
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $user = new User;

        $user->name = $args['name'];

        $user->email = $args['email'];

        $user->password = bcrypt($args['password']);

        $user->save();

        if (isset($args['roles'])) {
            $user->syncRoles($args['roles']);
        } else {
            $role = Role::where('name', '=', 'User')->first();
            $user->attachRole($role);
        }

        $user->save();

        return $user;
    }
}
