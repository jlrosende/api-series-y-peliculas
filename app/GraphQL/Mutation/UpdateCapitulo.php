<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 12:53
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\Capitulo;
use SeriesYPeliculas\Temporada;


class UpdateCapitulo
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $capiulo = Capitulo::find($args['id']);


        $capiulo->nombre = $args['nombre'];

        if (isset($args['sinopsis'])) {
            $capiulo->sinopsis = $args['sinopsis'];
        }

        if (isset($args['duracion'])) {
            $capiulo->duracion = date("H:i:s", strtotime($args['duracion']));
        }

        $temporada = Temporada::find($args['temporada']);
        $capiulo->temporada()->associate($temporada);

        $capiulo->save();

        return $capiulo;
    }
}
