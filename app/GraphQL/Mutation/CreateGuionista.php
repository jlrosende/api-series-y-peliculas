<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 17:18
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\Guionista;
use SeriesYPeliculas\Pelicula;
use SeriesYPeliculas\Persona;
use SeriesYPeliculas\Serie;

class CreateGuionista
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $guionista = new Guionista;

        $persona = Persona::find($args['persona']);
        $guionista->persona()->associate($persona);

        $guionista->save();

        if (isset($args['peliculas'])) {
            foreach ($args['peliculas'] as $id) {
                $pelicula = Pelicula::find($id);
                $guionista->peliculas()->attach($pelicula->id);
            }
        }

        if (isset($args['series'])) {
            foreach ($args['series'] as $id) {
                $serie = Serie::find($id);
                $guionista->series()->attach($serie->id);
            }
        }

        $guionista->save();

        return $guionista;
    }
}
