<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 14/2/18
 * Time: 12:50
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use Illuminate\Support\Facades\Storage;

use SeriesYPeliculas\Persona;

class UpdatePersona
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $persona = Persona::find($args['id']);

        if (isset($args['nombre'])) {
            $persona->nombre = $args['nombre'];
        }

        if (isset($args['foto'])) {
            if ($persona->foto) {
                Storage::disk('public')->delete($persona->foto);
            }
            $persona->foto = $args['foto']->store(
                'personas/' . $args['id'],
                'public'
            );
        }

        if (isset($args['fecha_nacimiento'])) {
            $persona->fecha_nacimiento = $args['fecha_nacimiento'];
        }

        if (isset($args['fecha_defuncion'])) {
            $persona->fecha_defuncion = $args['fecha_defuncion'];
        }

        if (isset($args['paises'])) {
            $persona->paises()->sync($args['paises']);
        }

        $persona->save();

        return $persona;
    }
}
