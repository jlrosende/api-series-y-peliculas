<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 6/2/18
 * Time: 0:43
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\Actor;
use SeriesYPeliculas\Pelicula;
use SeriesYPeliculas\Persona;
use SeriesYPeliculas\Serie;

class CreateActor
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $actor = new Actor;

        $persona = Persona::find($args['persona']);
        $actor->persona()->associate($persona);

        $actor->save();

        if (isset($args['peliculas'])) {
            foreach ($args['peliculas'] as $id) {
                $pelicula = Pelicula::find($id);
                $actor->peliculas()->attach($pelicula->id);
            }
        }

        if (isset($args['series'])) {
            foreach ($args['series'] as $id) {
                $serie = Serie::find($id);
                $actor->series()->attach($serie->id);
            }
        }

        $actor->save();

        return $actor;
    }
}
