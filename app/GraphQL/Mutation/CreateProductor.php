<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 14/2/18
 * Time: 13:07
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Pelicula;
use SeriesYPeliculas\Persona;
use SeriesYPeliculas\Productor;
use SeriesYPeliculas\Serie;

class CreateProductor
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $productor = new Productor;

        $persona = Persona::find($args['persona']);
        $productor->persona()->associate($persona);

        $productor->save();

        if (isset($args['peliculas'])) {
            foreach ($args['peliculas'] as $id) {
                $pelicula = Pelicula::find($id);
                $productor->peliculas()->attach($pelicula->id);
            }
        }

        if (isset($args['series'])) {
            foreach ($args['series'] as $id) {
                $serie = Serie::find($id);
                $productor->series()->attach($serie->id);
            }
        }

        $productor->save();

        return $productor;
    }
}
