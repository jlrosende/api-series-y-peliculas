<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 13:35
 */



namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\Director;
use SeriesYPeliculas\Pelicula;
use SeriesYPeliculas\Persona;
use SeriesYPeliculas\Serie;

class CreateDirector
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $director = new Director;

        $persona = Persona::find($args['persona']);
        $director->persona()->associate($persona);

        $director->save();

        if (isset($args['peliculas'])) {
            foreach ($args['peliculas'] as $id) {
                $pelicula = Pelicula::find($id);
                $director->peliculas()->attach($pelicula->id);
            }
        }

        if (isset($args['series'])) {
            foreach ($args['series'] as $id) {
                $serie = Serie::find($id);
                $director->series()->attach($serie->id);
            }
        }

        $director->save();

        return $director;
    }
}
