<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 15/2/18
 * Time: 15:41
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;


use SeriesYPeliculas\Serie;
use SeriesYPeliculas\Temporada;

class UpdateTemporada
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)


    {

        $temporada = Temporada::find($args['id']);

        $temporada->nombre = $args['nombre'];

        $serie = Serie::find($args['serie']);
        $temporada->serie()->associate($serie);

        $temporada->save();

        return $temporada;
    }
}
