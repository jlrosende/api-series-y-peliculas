<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 7/2/18
 * Time: 12:42
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;



use SeriesYPeliculas\BandaSonora;


class DeleteBandaSonora
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $banda_sonora = BandaSonora::findOrFail($args['id']);
        if ($banda_sonora->delete()) {
            return $banda_sonora;
        }
        return null;
    }
}
