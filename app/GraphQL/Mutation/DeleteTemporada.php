<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 7/2/18
 * Time: 13:12
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;



use SeriesYPeliculas\Temporada;


class DeleteTemporada
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $temporada = Temporada::findOrFail($args['id']);
        if ($temporada->delete()) {
            return $temporada;
        }
        return null;
    }
}
