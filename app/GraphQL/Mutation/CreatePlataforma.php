<?php

namespace SeriesYPeliculas\GraphQL\Mutation;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use SeriesYPeliculas\Plataforma;
use SeriesYPeliculas\Pelicula;
use SeriesYPeliculas\Serie;

class CreatePlataforma
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $plataforma = new Plataforma;

        $plataforma->nombre = $args['nombre'];

        $plataforma->url = $args['url'];

        if (isset($args['logo'])) {
            $plataforma->logo = $args['logo']->store(
                'plataformas/' . $args['id'],
                'public'
            );
        }

        if (isset($args['peliculas'])) {
            $plataforma->peliculas()->attach($args['peliculas']);
        }

        if (isset($args['series'])) {
            $plataforma->series()->attach($args['series']);
        }

        $plataforma->save();

        return $plataforma;
    }
}
