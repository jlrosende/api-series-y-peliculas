<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 17:24
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\Pais;

class UpdatePais
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $pais = Pais::find($args['id']);

        if (isset($args['nombre'])) {
            $pais->nombre = $args['nombre'];
        }
        if (isset($args['siglas'])) {
            $pais->siglas = $args['siglas'];
        }

        $pais->save();

        return $pais;
    }
}
