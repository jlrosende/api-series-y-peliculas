<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 7/2/18
 * Time: 12:56
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;



use SeriesYPeliculas\Pais;


class DeletePais
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $pais = Pais::findOrFail($args['id']);
        if ($pais->delete()) {
            return $pais;
        }
        return null;
    }
}
