<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 13:09
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\Compositor;
use SeriesYPeliculas\Persona;

class UpdateCompositor
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $compositor = Compositor::find($args['id']);

        $persona = Persona::find($args['persona']);
        $compositor->persona()->associate($persona);

        $compositor->save();

        return $compositor;
    }
}
