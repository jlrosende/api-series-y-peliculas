<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 13:35
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Director;
use SeriesYPeliculas\Persona;

class UpdateDirector
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $director = Director::find($args['id']);

        $persona = Persona::find($args['persona']);
        $director->persona()->associate($persona);

        if (isset($args['peliculas'])) {
            $director->peliculas()->sync($args['peliculas']);
        }

        if (isset($args['series'])) {
            $director->series()->sync($args['series']);
        }

        $director->save();

        return $director;
    }
}
