<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 5/2/18
 * Time: 12:48
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;



use SeriesYPeliculas\Pelicula;


class DeletePelicula
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $pelicula = Pelicula::findOrFail($args['id']);
        if ($pelicula->delete()) {
            return $pelicula;
        }
        return null;
    }
}
