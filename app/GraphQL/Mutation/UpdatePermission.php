<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 17:49
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;


use jeremykenedy\LaravelRoles\Models\Permission;

class UpdatePermission
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $permission = Permission::find($args['id']);

        $permission->name = $args['name'];

        $permission->slug = $args['slug'];

        if (isset($args['description'])) {
            $permission->description = $args['description'];
        }

        if (isset($args['model'])) {
            $permission->model = $args['model'];
        }

        $permission->save();

        return $permission;
    }
}
