<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 17:19
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;




use SeriesYPeliculas\Guionista;
use SeriesYPeliculas\Persona;

class UpdateGuionista
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $guionista = Guionista::find($args['id']);

        $persona = Persona::find($args['persona']);
        $guionista->persona()->associate($persona);

        if (isset($args['peliculas'])) {
            $guionista->peliculas()->sync($args['peliculas']);
        }

        if (isset($args['series'])) {
            $guionista->series()->sync($args['series']);
        }

        $guionista->save();

        return $guionista;
    }
}
