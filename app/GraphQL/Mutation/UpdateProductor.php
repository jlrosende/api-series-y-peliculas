<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 14/2/18
 * Time: 13:07
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\Persona;
use SeriesYPeliculas\Productor;

class UpdateProductor
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $productor = Productor::find($args['id']);

        $persona = Persona::find($args['persona']);
        $productor->persona()->associate($persona);

        if (isset($args['peliculas'])) {
            $productor->peliculas()->sync($args['peliculas']);
        }

        if (isset($args['series'])) {
            $productor->series()->sync($args['series']);
        }

        $productor->save();

        return $productor;
    }
}
