<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 8/2/18
 * Time: 17:27
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use Illuminate\Support\Facades\Storage;

use SeriesYPeliculas\Perfil;
use SeriesYPeliculas\User;

class CreatePerfil
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $perfil = new Perfil;

        $perfil->nombre = $args['nombre'];

        $perfil->apellidos = $args['apellidos'];

        $user = User::find($args['user']);
        $perfil->user()->associate($user);

        if (isset($args['avatar'])) {
            $perfil->avatar = $args['avatar']->store(
                'avatares/' . $args['user'],
                'public'
            );
        }

        if (isset($args['telefono'])) {
            $perfil->telefono = $args['telefono'];
        }

        if (isset($args['cpostal'])) {
            $perfil->cpostal = $args['cpostal'];
        }

        if (isset($args['direccion'])) {
            $perfil->direccion = $args['direccion'];
        }

        if (isset($args['imagen'])) {
            $perfil->imagen = $args['imagen'];
        }

        $perfil->save();

        return $perfil;
    }
}
