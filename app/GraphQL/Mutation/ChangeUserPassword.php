<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 14/2/18
 * Time: 13:23
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use jeremykenedy\LaravelRoles\Models\Role;
use Nuwave\Lighthouse\Exceptions\AuthenticationException;

class ChangeUserPassword
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $user = User::findOrFail($args['id']);


        if (isset($args['new_password']) && isset($args['old_password'])) {
            if (Hash::check($args['old_password'], $user->password)) {
                $user->password = bcrypt($args['new_password']);
            } else {
                throw new AuthenticationException('Incorrect old password');
            }
        }

        $user->save();

        return $user;
    }
}
