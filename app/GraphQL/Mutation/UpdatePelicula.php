<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 5/2/18
 * Time: 10:32
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\BandaSonora;
use SeriesYPeliculas\Pelicula;

use Illuminate\Support\Facades\Storage;

class UpdatePelicula
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $pelicula = Pelicula::find($args['id']);

        $pelicula->nombre = $args['nombre'];

        if (isset($args['anio'])) {
            $pelicula->anio = $args['anio'];
        }

        if (isset($args['sinopsis'])) {
            $pelicula->sinopsis = $args['sinopsis'];
        }

        if (isset($args['duracion'])) {
            $pelicula->duracion = date("H:i:s", strtotime($args['duracion']));
        }

        if (isset($args['cartel'])) {
            if ($pelicula->cartel) {
                Storage::disk('public')->delete($pelicula->cartel);
            }
            $pelicula->cartel = $args['cartel']->store(
                'peliculas/' . $args['id'],
                'public'
            );
        }

        if (isset($args['paises'])) {
            $pelicula->paises()->sync($args['paises']);
        }

        if (isset($args['generos'])) {
            $pelicula->generos()->sync($args['generos']);
        }

        if (isset($args['directores'])) {
            $pelicula->directores()->sync($args['directores']);
        }

        if (isset($args['actores'])) {
            $pelicula->actores()->sync($args['actores']);
        }

        if (isset($args['guionistas'])) {
            $pelicula->guionistas()->sync($args['guionistas']);
        }

        if (isset($args['productores'])) {
            $pelicula->productores()->sync($args['productores']);
        }

        if (isset($args['banda_sonora'])) {
            $banda_sonora = BandaSonora::findOrFail($args['banda_sonora']);
            $pelicula->banda_sonora()->associate($banda_sonora);
        }

        if (isset($args['plataformas'])) {
            $pelicula->plataformas()->sync($args['plataformas']);
        }

        $pelicula->save();

        return $pelicula;
    }
}
