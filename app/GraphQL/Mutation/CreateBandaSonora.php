<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 7/2/18
 * Time: 17:56
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use SeriesYPeliculas\BandaSonora;
use SeriesYPeliculas\Compositor;

class CreateBandaSonora
{

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $bandaSonora = new BandaSonora();

        $bandaSonora->nombre = $args['nombre'];

        if (isset($args['compositor'])) {
            $compositor = Compositor::find($args['compositor']);
            $bandaSonora->compositor()->associate($compositor);
        }

        $bandaSonora->save();

        return $bandaSonora;
    }
}
