<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 7/2/18
 * Time: 13:08
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;



use SeriesYPeliculas\Persona;


class DeletePersona
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $persona = Persona::findOrFail($args['id']);
        if ($persona->delete()) {
            return $persona;
        }
        return null;
    }
}
