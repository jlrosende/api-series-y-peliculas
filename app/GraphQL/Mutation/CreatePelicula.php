<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 30/8/17
 * Time: 10:20
 */

namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;

use Illuminate\Support\Facades\Storage;



use SeriesYPeliculas\Actor;
use SeriesYPeliculas\BandaSonora;
use SeriesYPeliculas\Director;
use SeriesYPeliculas\Genero;
use SeriesYPeliculas\Guionista;
use SeriesYPeliculas\Pais;
use SeriesYPeliculas\Pelicula;
use SeriesYPeliculas\Productor;
use SeriesYPeliculas\Plataforma;

class CreatePelicula
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $pelicula = new Pelicula;

        $pelicula->nombre = $args['nombre'];

        if (isset($args['anio'])) {
            $pelicula->anio = date($args['anio']);
        }

        if (isset($args['sinopsis'])) {
            $pelicula->sinopsis = $args['sinopsis'];
        }

        if (isset($args['duracion'])) {
            $pelicula->duracion = date("H:i:s", strtotime($args['duracion']));
        }

        if (isset($args['cartel'])) {
            $pelicula->cartel = $args['cartel']->store(
                'peliculas/' . $args['id'],
                'public'
            );
        }

        $pelicula->save();

        if (isset($args['paises'])) {
            foreach ($args['paises'] as $id) {
                $pais =  Pais::find($id);
                $pelicula->paises()->attach($pais->id);
            }
        }

        if (isset($args['generos'])) {
            foreach ($args['generos'] as $id) {
                $genero =  Genero::find($id);
                $pelicula->generos()->attach($genero->id);
            }
        }

        if (isset($args['directores'])) {
            foreach ($args['directores'] as $id) {
                $director =  Director::find($id);
                $pelicula->directores()->attach($director->id);
            }
        }

        if (isset($args['actores'])) {
            foreach ($args['actores'] as $id) {
                $actor =  Actor::find($id);
                $pelicula->actores()->attach($actor->id);
            }
        }

        if (isset($args['guionistas'])) {
            foreach ($args['guionistas'] as $id) {
                $guionista =  Guionista::find($id);
                $pelicula->guionistas()->attach($guionista->id);
            }
        }

        if (isset($args['productores'])) {
            foreach ($args['productores'] as $id) {
                $productor =  Productor::find($id);
                $pelicula->productores()->attach($productor->id);
            }
        }

        if (isset($args['plataformas'])) {
            foreach ($args['plataformas'] as $id) {
                $plataforma =  Plataforma::find($id);
                $pelicula->plataformas()->attach($plataforma->id);
            }
        }

        if (isset($args['banda_sonora'])) {
            $banda_sonora = BandaSonora::find($args['banda_sonora']);
            $pelicula->banda_sonora()->associate($banda_sonora);
        }

        $pelicula->save();

        return $pelicula;
    }
}
