<?php

/**
 * Created by PhpStorm.
 * User: rosende95
 * Date: 7/2/18
 * Time: 13:10
 */


namespace SeriesYPeliculas\GraphQL\Mutation;

use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use GraphQL\Type\Definition\ResolveInfo;



use jeremykenedy\LaravelRoles\Models\Role;


class DeleteRole
{



    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $role = Role::findOrFail($args['id']);
        if ($role->delete()) {
            return $role;
        }
        return null;
    }
}
