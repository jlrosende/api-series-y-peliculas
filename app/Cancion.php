<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Cancion extends Model
{
    protected $table = 'canciones';

    public function bandaSonora(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\BandaSonora');
    }
}
