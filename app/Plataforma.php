<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Plataforma extends Model
{
    protected $table = 'plataformas';

    public function peliculas(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Pelicula', 'plataforma_pelicula')->withPivot(['url', 'inicio_disponible', 'fin_disponible']);
    }

    public function series(): BelongsToMany
    {
        return $this->belongsToMany('SeriesYPeliculas\Serie', 'plataforma_serie')->withPivot(['url', 'inicio_disponible', 'fin_disponible']);
    }
}
