<?php

namespace SeriesYPeliculas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Capitulo extends Model
{
    protected $table = 'capitulos';

    public function temporada(): BelongsTo
    {
        return $this->belongsTo('SeriesYPeliculas\Temporada');
    }
}
