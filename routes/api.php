<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

/*
// Autentification user
Route::post('auth',         'ApiAuthController@authenticate');
// Register user
Route::post('register',     'UserController@store');

// App routes
Route::group(['prefix' => 'v1', 'middleware' => ['jwt.auth', 'jwt.refresh']], function () {
    Route::group(['middleware' => 'level:1'], function () {
        Route::resource('actores',          'ActorController',          ['only' => ['index', 'show']]);
        Route::resource('bandassonoras',    'BandaSonoraController',    ['only' => ['index', 'show']]);
        Route::resource('canciones',        'CancionController',        ['only' => ['index', 'show']]);
        Route::resource('capitulos',        'CapituloController',       ['only' => ['index', 'show']]);
        Route::resource('compositores',     'CompositorController',     ['only' => ['index', 'show']]);
        Route::resource('directores',       'DirectorController',       ['only' => ['index', 'show']]);
        Route::resource('generos',          'GeneroController',         ['only' => ['index', 'show']]);
        Route::resource('guionistas',       'GuionistaController',      ['only' => ['index', 'show']]);
        Route::resource('directores',       'DirectorController',       ['only' => ['index', 'show']]);
        Route::resource('paises',           'PaisController',           ['only' => ['index', 'show']]);
        Route::resource('peliculas',        'PeliculaController',       ['only' => ['index', 'show']]);
        Route::resource('series',           'SerieController',          ['only' => ['index', 'show']]);
        Route::resource('temporadas',       'TemporadaController',      ['only' => ['index', 'show']]);
    });

    Route::group(['middleware' => 'role:admin'], function () {
        Route::resource('actores',          'ActorController',          ['except' => ['index', 'show', 'create']]);
        Route::resource('bandassonoras',    'BandaSonoraController',    ['except' => ['index', 'show', 'create']]);
        Route::resource('canciones',        'CancionController',        ['except' => ['index', 'show', 'create']]);
        Route::resource('capitulos',        'CapituloController',       ['except' => ['index', 'show', 'create']]);
        Route::resource('compositores',     'CompositorController',     ['except' => ['index', 'show', 'create']]);
        Route::resource('directores',       'DirectorController',       ['except' => ['index', 'show', 'create']]);
        Route::resource('generos',          'GeneroController',         ['except' => ['index', 'show', 'create']]);
        Route::resource('guionistas',       'GuionistaController',      ['except' => ['index', 'show', 'create']]);
        Route::resource('directores',       'DirectorController',       ['except' => ['index', 'show', 'create']]);
        Route::resource('paises',           'PaisController',           ['except' => ['index', 'show', 'create']]);
        Route::resource('peliculas',        'PeliculaController',       ['except' => ['index', 'show', 'create']]);
        Route::resource('series',           'SerieController',          ['except' => ['index', 'show', 'create']]);
        Route::resource('temporadas',       'TemporadaController',      ['except' => ['index', 'show', 'create']]);
    });

    // User routes
    Route::group(['middleware' => 'level:2'], function () {
        Route::resource('usuarios',         'UserController',           ['except' => ['create']]);
    });
});
*/
